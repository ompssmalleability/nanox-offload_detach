#
# SYNOPSIS
#
#   AX_CHECK_SLURM
#
# DESCRIPTION
#
#   Check whether SLURM path to the headers and libraries are correctly specified.
#
# LICENSE
#
#   Copyright (c) 2015 Marco D'Amico <marco.damico@bsc.es>
#   
#   This program is free software: you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   This program is distributed in the hope that it will be useful, but
#   WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
#   Public License for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program. If not, see <http://www.gnu.org/licenses/>.
#
#   As a special exception, the respective Autoconf Macro's copyright owner
#   gives unlimited permission to copy, distribute and modify the configure
#   scripts that are the output of Autoconf when processing the Macro. You
#   need not follow the terms of the GNU General Public License when using
#   or distributing such scripts, even though portions of the text of the
#   Macro appear in them. The GNU General Public License (GPL) does govern
#   all other use of the material that constitutes the Autoconf Macro.
#
#   This special exception to the GPL applies to versions of the Autoconf
#   Macro released by the Autoconf Archive. When you make and distribute a
#   modified version of the Autoconf Macro, you may extend this special
#   exception to the GPL to apply to your modified version as well.

AC_DEFUN([AX_CHECK_SLURM],
[
AC_PREREQ(2.59)dnl for _AC_LANG_PREFIX

#Check if an SLURM implementation is installed.
AC_ARG_WITH(SLURM,
[AS_HELP_STRING([--with-SLURM,--with-SLURM=PATH],
                [search in system directories or specify prefix directory for installed SLURM package.])])
AC_ARG_WITH(SLURM-include,
[AS_HELP_STRING([--with-SLURM-include=PATH],
                [specify directory for installed SLURM include files])])
AC_ARG_WITH(SLURM-lib,
[AS_HELP_STRING([--with-SLURM-lib=PATH],
                [specify directory for the installed SLURM library])])


# Check if user specifically requested this package.
# It will generate errors if we are not able to find headers/libs
if test "x$with_SLURM$with_SLURM_include$with_SLURM_lib" != x; then
  user_requested="yes"
else
  user_requested="no"
fi

# If the user specifies --with-SLURM, $with_SLURM value will be 'yes'
#                       --without-SLURM, $with_SLURM value will be 'no'
#                       --with-SLURM=somevalue, $with_SLURM value will be 'somevalue'
if [[[ ! "x$with_SLURM" =~  x(yes|no|)$ ]]]; then
  SLURMinc="-I$with_SLURM/include"
  SLURMlib="-L$with_SLURM/lib -Wl,-rpath=$with_SLURM/lib"
fi

if test $with_SLURM_include; then
  SLURMinc="-I$with_SLURM_include"
fi

if test $with_SLURM_lib; then
  SLURMlib="-L$with_SLURM_lib"
fi

# This is fulfilled even if $with_SLURM="yes" 
# This happens when user leaves --with-value empty
# In this case, both SLURMinc and SLURMlib will be empty
# so the test should search in default locations and LD_LIBRARY_PATH
if test "x$with_SLURM" != xno; then
    #tests if provided headers and libraries are usable and correct
    bak_CFLAGS="$CFLAGS"
    bak_CxXFLAGS="$CXXFLAGS"
    bak_CPPFLAGS="$CPPFLAGS"
    bak_LIBS="$LIBS"
    bak_LDFLAGS="$LDFLAGS"

    CFLAGS=
    CXXFLAGS=
    CPPFLAGS=$SLURMinc
    LIBS=
    LDFLAGS=$SLURMlib

    # One of the following two header files has to exist
    AC_CHECK_HEADERS([slurm/slurm_errno.h slurm/slurm.h], [SLURM=yes], [SLURM=no])
    # Look for slurm_allocate_resources_blocking function in libslurm.so libraries
    if test x$SLURM = xyes; then
        AC_SEARCH_LIBS([slurm_allocate_resources_blocking],
                  [slurm],
                  [SLURM=yes],
                  [SLURM=no])
    fi

    SLURMlib="$SLURMlib -DSLURM_IS_USED  $LIBS"

    CFLAGS="$bak_CFLAGS"
    CXXFLAGS="$bak_CXXFLAGS"
    CPPFLAGS="$bak_CPPFLAGS"
    LIBS="$bak_LIBS"
    LDFLAGS="$bak_LDFLAGS"

    if test x$user_requested = xyes -a x$SLURM != xyes; then
        AC_MSG_ERROR([
------------------------------
SLURM path was not correctly specified. 
Please, check that the provided directories are correct.
------------------------------])
    fi
fi

AM_CONDITIONAL([SLURM_SUPPORT],[test x$SLURM = xyes])

AC_SUBST([SLURM])
AC_SUBST([SLURMinc])
AC_SUBST([SLURMlib])

])dnl AX_CHECK_SLURM
