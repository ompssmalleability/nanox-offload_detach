/*************************************************************************************/
/*      Copyright 2015 Barcelona Supercomputing Center                               */
/*                                                                                   */
/*      This file is part of the NANOS++ library.                                    */
/*                                                                                   */
/*      NANOS++ is free software: you can redistribute it and/or modify              */
/*      it under the terms of the GNU Lesser General Public License as published by  */
/*      the Free Software Foundation, either version 3 of the License, or            */
/*      (at your option) any later version.                                          */
/*                                                                                   */
/*      NANOS++ is distributed in the hope that it will be useful,                   */
/*      but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*      GNU Lesser General Public License for more details.                          */
/*                                                                                   */
/*      You should have received a copy of the GNU Lesser General Public License     */
/*      along with NANOS++.  If not, see <http://www.gnu.org/licenses/>.             */
/*************************************************************************************/

#ifndef _NANOS_MPI_REMOTE_NODE
#define _NANOS_MPI_REMOTE_NODE

#include "mpi.h"
#include "atomic_decl.hpp"
#include "mpiremotenode_decl.hpp"
#include "config.hpp"
#include "mpidevice.hpp"
#include "mpithread.hpp"
#include "cachedaccelerator.hpp"
#include "copydescriptor_decl.hpp"
#include "processingelement.hpp"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>

#define HOSTLEN 128

#define STR_SIZE 2048
#define PORT 8262

using namespace nanos;
using namespace ext;

static double wall_time(void);
static int getJobidSlurm(void);
static int getPeriodEnv(void);

Lock MPIRemoteNode::_taskLock;
pthread_cond_t MPIRemoteNode::_taskWait; //! Condition variable to wait for completion
pthread_mutex_t MPIRemoteNode::_taskMutex; //! Mutex to access the completion 
std::list<int> MPIRemoteNode::_pendingTasksQueue;
std::list<int> MPIRemoteNode::_pendingTaskParentsQueue;
std::vector<MPI_Datatype*> MPIRemoteNode::_taskStructsCache;
bool MPIRemoteNode::_initialized = false;
bool MPIRemoteNode::_disconnectedFromParent = false;
int MPIRemoteNode::_currentTaskParent = -1;
int MPIRemoteNode::_currProcessor = 0;
int MPIRemoteNode::_nDatasent = 0;
bool MPIRemoteNode::_detached = false;
bool MPIRemoteNode::_detachAlloc = false;
//char* MPIRemoteNode::_currNodenames;
int MPIRemoteNode::_currNodes = -1;
int MPIRemoteNode::_shrinkFinished = 1;
int MPIRemoteNode::_actionRMS = -1;
int MPIRemoteNode::_expandJobId = getJobidSlurm();
char *MPIRemoteNode::_managementHost;
pthread_t MPIRemoteNode::_socketThread;
pthread_t *MPIRemoteNode::_pthreads_ids;
double MPIRemoteNode::_walltime = wall_time();
int MPIRemoteNode::_period = getPeriodEnv(); //in seconds
//int MPIRemoteNode::_period = 0;

bool MPIRemoteNode::checkTimePeriod() {
    bool res = true;
    if (_period) {
        if ((wall_time() - _walltime) < _period)
            res = false;
    }
    if (res)
        _walltime = wall_time();
    return res;
}

int MPIRemoteNode::getPeriod() {

    return _period;
}

double MPIRemoteNode::getWalltime() {

    return _walltime;
}

bool MPIRemoteNode::getDetached() {

    return _detached;
}

void MPIRemoteNode::setDetached() {

    _detached = true;
}

int MPIRemoteNode::getNDatasent() {

    return _nDatasent;
}

void MPIRemoteNode::incrementNDatasent() {

    pthread_mutex_lock(&_taskMutex);
    _nDatasent++;
    pthread_mutex_unlock(&_taskMutex);
}

Lock& MPIRemoteNode::getTaskLock() {
    return _taskLock;
}

int MPIRemoteNode::getQueueCurrTaskIdentifier() {
    return _pendingTasksQueue.front();
}

int MPIRemoteNode::getQueueCurrentTaskParent() {
    return _pendingTaskParentsQueue.front();
}

int MPIRemoteNode::getCurrentTaskParent() {
    return _currentTaskParent;
}

void MPIRemoteNode::setCurrentTaskParent(int parent) {
    _currentTaskParent = parent;
}

int MPIRemoteNode::getCurrentProcessor() {
    return _currProcessor++;
}

void MPIRemoteNode::testTaskQueueSizeAndLock() {
    pthread_mutex_lock(&_taskMutex);
    if (_pendingTasksQueue.size() == 0) {
        pthread_cond_wait(&_taskWait, &_taskMutex);
    }
    pthread_mutex_unlock(&_taskMutex);
}

void MPIRemoteNode::addTaskToQueue(int task_id, int parentId) {
    pthread_mutex_lock(&_taskMutex);
    _pendingTasksQueue.push_back(task_id);
    _pendingTaskParentsQueue.push_back(parentId);
    pthread_cond_signal(&_taskWait);
    pthread_mutex_unlock(&_taskMutex);
}

void MPIRemoteNode::removeTaskFromQueue() {
    pthread_mutex_lock(&_taskMutex);
    _pendingTasksQueue.pop_front();
    _pendingTaskParentsQueue.pop_front();
    pthread_mutex_unlock(&_taskMutex);
}

bool MPIRemoteNode::getDisconnectedFromParent() {
    return _disconnectedFromParent;
}

////////////////////////////////
//Auxiliar filelock routines////
////////////////////////////////

/*! Try to get lock. Return its file descriptor or -1 if failed.
 *
 *  @param lockName Name of file used as lock (i.e. '/var/lock/myLock').
 *  @return File descriptor of lock file, or -1 if failed.
 */
static int tryGetLock(char const *lockName) {
    int fd = open(lockName, O_RDWR | O_CREAT, 0666);
    struct flock lock;
    lock.l_type = F_WRLCK; /* Test for any lock on any part of file. */
    lock.l_start = 0;
    lock.l_whence = SEEK_SET;
    lock.l_len = 0;
    if (fcntl(fd, F_SETLKW, &lock) < 0) { /* Overwrites lock structure with preventors. */
        fd = -1;
        close(fd);
    }
    return fd;
}

/*! Release the lock obtained with tryGetLock( lockName ).
 *
 *  @param fd File descriptor of lock returned by tryGetLock( lockName ).
 *  @param lockName Name of file used as lock (i.e. '/var/lock/myLock').
 */
static void releaseLock(int fd, char const *lockName) {
    if (fd < 0)
        return;
    struct flock lock;
    lock.l_type = F_WRLCK; /* Test for any lock on any part of file. */
    lock.l_start = 0;
    lock.l_whence = SEEK_SET;
    lock.l_len = 0;
    fcntl(fd, F_UNLCK, &lock); /* Overwrites lock structure with preventors. */
    //remove( lockName );
    close(fd);
}

static void readableHostlist(char * nameExp, char * readableNames) {
    hostlist_t hl = slurm_hostlist_create(nameExp);
    char *host = slurm_hostlist_shift(hl);
    sprintf(readableNames, "%s", host);
    host = slurm_hostlist_shift(hl);
    while (host != NULL) {

        sprintf(readableNames, "%s,%s", readableNames, host);
        host = slurm_hostlist_shift(hl);
    }
    slurm_hostlist_destroy(hl);
}

//Return a comma-separated string of the first "counter" hosts.

static void readableHostlistTruncated(char * nameExp, char * readableNames, int counter) {
    int cnt = 0;
    hostlist_t hl = slurm_hostlist_create(nameExp);
    char *host = slurm_hostlist_shift(hl);
    sprintf(readableNames, "%s", host);
    host = slurm_hostlist_shift(hl);
    while (cnt < counter - 1 && host != NULL) {

        sprintf(readableNames, "%s,%s", readableNames, host);
        host = slurm_hostlist_shift(hl);
        cnt++;
    }
    slurm_hostlist_destroy(hl);
}

static void error(const char *msg) {

    perror(msg);
    exit(1);
}

void * listener(void * n_deallocs);

void * connection_handler(void *params);

struct thread_socket {
    int sockfd;
    void * cli_addr;
    int *clilen;
};

void * async_sched(void *params);

struct thread_sched {
    int job_id;
    int *min;
    int *max;
    int *preference;
    int *step;
    int *currentNodes;
    int *ptrCurrNodes;
    int *ptrActionRMS;
    char *ptrManagementNode;
    int *ptrExpandJob;
    int *ptrShrinkFinished;
};

struct select_jobinfo {
    uint32_t job_id;
    uint32_t action;
    uint32_t min;
    uint32_t max;
    uint32_t preference;
    uint32_t step;
    uint32_t currentNodes;
    uint32_t resultantNodes;
    char hostlist[STR_SIZE];
};

static double wall_time(void) {
    struct timeval val;
    struct timezone zone;
    gettimeofday(&val, &zone);

    return (double) val.tv_sec + (double) val.tv_usec * 1e-6;
}

static int getJobidSlurm(void) {
    return atoi(getenv("SLURM_JOBID"));
}

static int getPeriodEnv(void) {
    int val = 0;
    if (char * var = getenv("NANOX_SCHED_PERIOD")) {
        val = atoi(var);
        if (val < 0)
            val = 0;
    }
    return val;
}

#endif
