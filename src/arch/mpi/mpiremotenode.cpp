/*************************************************************************************/
/*      Copyright 2015 Barcelona Supercomputing Center                               */
/*                                                                                   */
/*      This file is part of the NANOS++ library.                                    */
/*                                                                                   */
/*      NANOS++ is free software: you can redistribute it and/or modify              */
/*      it under the terms of the GNU Lesser General Public License as published by  */
/*      the Free Software Foundation, either version 3 of the License, or            */
/*      (at your option) any later version.                                          */
/*                                                                                   */
/*      NANOS++ is distributed in the hope that it will be useful,                   */
/*      but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*      GNU Lesser General Public License for more details.                          */
/*                                                                                   */
/*      You should have received a copy of the GNU Lesser General Public License     */
/*      along with NANOS++.  If not, see <http://www.gnu.org/licenses/>.             */
/*************************************************************************************/

#include "mpiremotenode.hpp"
#include "schedule.hpp"
#include "debug.hpp"
#include "config.hpp"
#include "mpithread.hpp"
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include "mpi.h"

#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <string>
#include <sstream>
#include <vector>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#undef SO_REUSEPORT
#define SO_REUSEPORT 256

#ifdef SLURM_IS_USED
#include "slurm/slurm.h"
#include "slurm/slurm_errno.h"
extern "C" int slurm_hostlist_delete(hostlist_t hl, const char *hosts);
extern "C" int slurm_hostlist_delete_host(hostlist_t ht, const char *hostname);
#endif


using namespace nanos;
using namespace nanos::ext;

extern __attribute__((weak)) int ompss_mpi_masks[];
extern __attribute__((weak)) unsigned int ompss_mpi_filenames[];
extern __attribute__((weak)) unsigned int ompss_mpi_file_sizes[];
extern __attribute__((weak)) unsigned int ompss_mpi_file_ntasks[];
extern __attribute__((weak)) void *ompss_mpi_func_pointers_host[];
extern __attribute__((weak)) void *ompss_mpi_func_pointers_dev[];

int gmin;
int gmax;
int gpref;
int gstep;
int gnodes;
bool state = true;
bool waiting = true;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

bool MPIRemoteNode::executeTask(int taskId) {
    bool ret = false;
    //printf("(sergio)[%d]: %s(%s,%d) - task %d\n", getpid(), __FILE__, __func__, __LINE__, taskId);
    if (taskId == TASK_END_PROCESS) {
        nanosMPIFinalize();
        nanos::ext::MPIRemoteNode::getTaskLock().release();
        ret = true;
    } else {
        void (* function_pointer)() = (void (*)()) ompss_mpi_func_pointers_dev[taskId];
        //nanos::MPIDevice::taskPreInit();
        function_pointer();
        //nanos::MPIDevice::taskPostFinish();
    }
    return ret;
}

int MPIRemoteNode::nanosMPIWorker() {
    bool finalize = false;
    while (!finalize) {
        //Acquire twice and block until cache thread unlocks
        testTaskQueueSizeAndLock();
        setCurrentTaskParent(getQueueCurrentTaskParent());
        finalize = executeTask(getQueueCurrTaskIdentifier());
        removeTaskFromQueue();
    }
    return 0;
}

void MPIRemoteNode::preInit() {
    nanosMPIInit(0, 0, MPI_THREAD_MULTIPLE, 0);
    nanos::ext::MPIRemoteNode::nanosSyncDevPointers(ompss_mpi_masks, ompss_mpi_filenames, ompss_mpi_file_sizes, ompss_mpi_file_ntasks, ompss_mpi_func_pointers_dev);
}

void MPIRemoteNode::mpiOffloadSlaveMain() {
    nanos::MPIDevice::remoteNodeCacheWorker();
    exit(0);
}

int MPIRemoteNode::ompssMpiGetFunctionIndexHost(void* func_pointer) {
    int i;
    //This function WILL find the pointer, if it doesnt, program would crash anyways so I won't limit it
    for (i = 0; ompss_mpi_func_pointers_host[i] != func_pointer; i++) {
    }
    return i;
}

int MPIRemoteNode::ompssMpiGetFunctionIndexDevice(void* func_pointer) {
    int i;
    //This function WILL find the pointer, if it doesnt, program would crash anyways so I won't limit it
    for (i = 0; ompss_mpi_func_pointers_dev[i] != func_pointer; i++) {
    }
    return i;
}

/**
 * Statics (mostly external API adapters provided to user or used by mercurium) begin here
 */

/**
 * All this tasks redefine nanox messages
 */
void MPIRemoteNode::nanosMPIInit(int *argc, char ***argv, int userRequired, int* userProvided) {
    if (_initialized) return;
    NANOS_MPI_CREATE_IN_MPI_RUNTIME_EVENT(ext::NANOS_MPI_INIT_EVENT);
    verbose0("loading MPI support");
    //Unless user specifies otherwise, enable blocking mode in MPI
    if (getenv("I_MPI_WAIT_MODE") == NULL) putenv(const_cast<char*> ("I_MPI_WAIT_MODE=1"));

    //If we are not offload slaves, initialice MPI plugin
    bool imSlave = getenv("OMPSS_OFFLOAD_SLAVE") != NULL;
    if (!imSlave) {
        if (!sys.loadPlugin("arch-mpi"))
            fatal0("Couldn't load MPI support");
    }

    _initialized = true;
    int provided;
    //If user provided a null pointer, we'll a value for internal checks
    if (userProvided == NULL) userProvided = &provided;

    int initialized;
    MPI_Initialized(&initialized);
    //In case it was already initialized (shouldn't happen, since we theorically "rename" the calls with mercurium), we won't try to do so
    //We'll trust user criteria, but show a warning
    if (!initialized) {
        if (userRequired != MPI_THREAD_MULTIPLE) {
            warning0("Initializing MPI with MPI_THREAD_MULTIPLE instead of user required mode, this is a requeriment for OmpSs offload");
        }
        MPI_Init_thread(argc, argv, MPI_THREAD_MULTIPLE, userProvided);
    } else {
        //Do not initialise, but check thread level and return the right provided value to the user
        MPI_Query_thread(userProvided);
    }
    int myRank;
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

    //Remove possible trashfiles from other executions
    if (myRank == 0 && !imSlave) {
        std::string lockname = "./.ompssOffloadLock";
        remove(const_cast<char*> (lockname.c_str()));
        if (!nanos::ext::MPIProcessor::getMpiControlFile().empty()) remove(const_cast<char*> (nanos::ext::MPIProcessor::getMpiControlFile().c_str()));
    }


    nanos::MPIDevice::initMPICacheStruct();

    NANOS_MPI_CLOSE_IN_MPI_RUNTIME_EVENT;
}

void MPIRemoteNode::nanosMPIFinalize() {
    //printf("(sergio)[%d]: %s(%s,%d) exiting\n", getpid(), __FILE__, __func__, __LINE__);
    MPI_Type_free(&MPIDevice::cacheStruct);
    MPIDevice::cacheStruct = MPI_DATATYPE_NULL;
    exit(0);

    NANOS_MPI_CREATE_IN_MPI_RUNTIME_EVENT(ext::NANOS_MPI_FINALIZE_EVENT);
    for (std::vector<MPI_Datatype*>::iterator datatype_it = _taskStructsCache.begin();
            datatype_it != _taskStructsCache.end(); datatype_it++) {
        // If some offload tasks were not used by this process,
        // some entries may be null.
        // A possible fix would be to use a map/unordered_map
        // instead of a vector of MPI_Datatype*
        if (*datatype_it != NULL) {
            MPI_Type_free(*datatype_it);
            delete *datatype_it;
        }
    }
    MPI_Type_free(&MPIDevice::cacheStruct);
    MPIDevice::cacheStruct = MPI_DATATYPE_NULL;

    //this exit is performed by the masters and the slaves, but apart from that, the masters also execute the exit in finish()
    //MPIRemoteNode::DMRDetachAlloc();
    //exit(0);

    int mpi_finalized;
    MPI_Finalized(&mpi_finalized);

    NANOS_MPI_CLOSE_IN_MPI_RUNTIME_EVENT;
    if (!mpi_finalized) {
        //Free every node before finalizing
        DEEP_Booster_free(NULL, -1);
        // In the case of slave processes,
        // disconnect from parent communicator
        MPI_Comm parent;
        MPI_Comm_get_parent(&parent);
        if (parent != MPI_COMM_NULL)
            MPI_Comm_disconnect(&parent);
        MPI_Finalize();
    }
}

//TODO: Finish implementing shared memory
#define N_FREE_SLOTS 10

void MPIRemoteNode::unifiedMemoryMallocHost(size_t size, MPI_Comm communicator) {
    //    int comm_size;
    //    MPI_Comm_remote_size(communicator,&comm_size);
    //    void* proposedAddr;
    //    bool sucessMalloc=false;
    //    while (!sucessMalloc) {
    //        proposedAddr=getFreeVirtualAddr(size);
    //        sucessMalloc=mmapOfAddr(proposedAddr);
    //    }
    //    //Fast mode: Try sending one random address and hope its free in every node
    //    cacheOrder newOrder;
    //    newOrder.opId=OPID_UNIFIED_MEM_REQ;
    //    newOrder.size=size;
    //    newOrder.hostAddr=(uint16_t)proposedAddr;
    //    for (int i=0; i<comm_size ; i++) {
    //       nanos::ext::MPIRemoteNode::nanosMPISend(&newOrder, 1, nanos::MPIDevice::cacheStruct, i, TAG_CACHE_ORDER, communicator);
    //    }
    //    int totalValids;
    //    nanos::ext::MPIRemoteNode::nanosMPIRecv(&totalValids, 1, MPI_INT, 0, TAG_UNIFIED_MEM, communicator, MPI_STATUS_IGNORE);
    //    if (totalValids!=comm_size) {
    //        //Fast mode failed, enter safe mode
    //        munmapOfAddr(proposedAddr);
    //        uint64_t finalPtr;
    //        uint64_t freeSpaces[N_FREE_SLOTS*2];
    //        getFreeSpacesArr(freeSpaces,N_FREE_SLOTS*2);
    //        MPI_Status status;
    //        ptrArr[i]=freeSpaces;
    //        arrLength[i]=N_FREE_SLOTS;
    //        sizeArr[i]=freeSpaces+arrLength[i];
    //        //Gather from everyone in the communicator
    //        //MPI_Gather could be an option, but this is not a performance critical routine
    //        //and would limit the sizes to a fixed size
    //        for (int i=0; i<comm_size; ++i) {
    //           MPI_Probe(parentRank, TAG_UNIFIED_MEM, parentcomm, &status);
    //TODO: FREE THIS MALLOCS
    //           localArr= (uint64_t*) malloc(status.count*sizeof(uint64_t));
    //           ptrArr[i]=localArr;
    //           arrLength[i]=status.count/2;
    //           sizeArr[i]=localArr+arrLength[i];
    //           nanos::ext::MPIRemoteNode::nanosMPIRecv(&localArr, status.count, MPI_LONG, i, TAG_UNIFIED_MEM, communicator);
    //        }
    //        //Now intersect all the free spaces we got...
    //        std::map<uint64_t,char> blackList;
    //        bool sucess=false;
    //        while (!sucess) {
    //            finalPtr=getFreeChunk(comm_size+1, ptrArr, sizeArr, arrLength, order.size, blackList);
    //            bool sucess=mmapOfAddr(finalPtr);
    //            if (sucess) {
    //                for (int i=0; i<comm_size ; i++) {
    //                   nanos::ext::MPIRemoteNode::nanosMPISend(&finalPtr, 1, MPI_LONG, i, TAG_UNIFIED_MEM, communicator);
    //                }
    //                int totalValids;
    //                nanos::ext::MPIRemoteNode::nanosMPIRecv(&totalValids, 1, MPI_INT, 0, TAG_UNIFIED_MEM, communicator, MPI_STATUS_IGNORE);
    //                if (totalValids!=comm_size) {
    //                    munmapOfAddr(finalPtr);
    //                    sucess=false;
    //                }
    //            }
    //            if (!sucess) blackList.insert(std::make_pair<uint64_t,char>(finalPtr,1));
    //        }
    //    }
}

void MPIRemoteNode::unifiedMemoryMallocRemote(cacheOrder& order, int parentRank, MPI_Comm parentcomm) {
    //    int comm_size;
    //    int localPositives=0;
    //    int totalPositives=0;
    //    MPI_Comm_size(MPI_COMM_WORLD,&comm_size);
    //    bool hostProposalIsFree=true;
    //    hostProposalIsFree=mmapOfAddr(order.hostAddr);
    //    localPositives+=(int) hostProposalIsFree;
    //    MPI_Allreduce(&localPositives, &totalPositives, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    //    if (rank==0) {
    //        nanos::ext::MPIRemoteNode::nanosMPISend(&totalPositives, 1, MPI_INT, parentRank, TAG_UNIFIED_MEM, parentcomm);
    //    }
    //    if (totalPositives!=localPositives) {
    //        //Some node failed doing malloc, entre safe mode
    //        munmapOfAddr(order.hostAddr);
    //        unifiedMemoryMallocRemoteSafe(order,parentRank,parentcomm);
    //    }
}

void MPIRemoteNode::unifiedMemoryMallocRemoteSafe(cacheOrder& order, int parentRank, MPI_Comm parentcomm) {
    //    uint64_t freeSpaces[N_FREE_SLOTS*2];
    //    int comm_size;
    //    int localPositives=0;
    //    int totalPositives=-1;
    //    //Send my array of free memory slots to the master
    //    MPI_Comm_size(MPI_COMM_WORLD,&comm_size);
    //    uint64_t finalPtr;
    //    getFreeSpacesArr(freeSpaces,N_FREE_SLOTS*2);
    //    nanos::ext::MPIRemoteNode::nanosMPISend(freespaces, sizeof(freeSpaces), MPI_LONG, parentRank, TAG_UNIFIED_MEM, parentcomm);
    //    //Keep receiving addresses from the master until every node could malloc a common address
    //    while (totalPositives!=localPositives) {
    //        totalPositives=0;
    //        localPositives=0;
    //        //Wait for the answer of the proposed/valid pointer
    //        nanos::ext::MPIRemoteNode::nanosMPIRecv(&finalPtr, 1, MPI_LONG, 0, TAG_UNIFIED_MEM, communicator, MPI_STATUS_IGNORE);
    //
    //        bool hostProposalIsFree=true;
    //        hostProposalIsFree=mmapOfAddr(finalPtr);
    //        localPositives+=(int) hostProposalIsFree;
    //        MPI_Allreduce(&localPositives, &totalPositives, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    //        if (rank==0) {
    //            nanos::ext::MPIRemoteNode::nanosMPISend(&totalPositives, 1, MPI_INT, parentRank, TAG_UNIFIED_MEM, parentcomm);
    //        }
    //        if (totalPositives!=localPositives) munmapOfAddr(finalPtr);
    //    }
}

uint64_t MPIRemoteNode::getFreeChunk(int arraysLength, uint64_t** arrOfPtr,
        uint64_t** sizeArr, int** arrLength, size_t chunkSize, std::map<uint64_t, char>& blackList) {
    //    uint64_t result=0;
    //    //TODO: Improve this implementation (brute force w/ many stop conditions + binary search right now)
    //    //For every free chunk in each node, check if it's available in every other node
    //    for (int masterSpaceNum=0; masterSpaceNum<arraysLength && resul!=0; ++masterSpaceNum) {
    //        uint64_t* masterPtrArr=arrOfPtr[masterSpaceNum];
    //        uint64_t* masterSizeArr=sizeArr[masterSpaceNum];
    //        int masterArrLength=arrLength[masterSpaceNum];
    //        for (int i=masterArrLength; i>=0 && resul!=0 ; --i) {
    //            uint64_t masterPtr=masterPtrArr[i];
    //            uint64_t masterSize=masterSizeArr[i];
    //            if (masterSize>=chunkSize && blackList.count(masterPtr)==0) {
    //                //Check if this pointer has free space
    //                bool isAvaiableInAllSpaces=true;
    //                for (int slaveSpaceNum=0; slaveSpaceNum<arraysLength && isAvaiableInAllSpaces; ++slaveSpaceNum) {
    //                    bool spaceHasFreeChunk=false;
    //                    uint64_t* slavePtrArr=arrOfPtr[slaveSpaceNum];
    //                    uint64_t* slaveSizeArr=sizeArr[slaveSpaceNum];
    //                    unsigned last=(unsigned)arrLength[slaveSpaceNum];
    //                    unsigned min=0;
    //                    unsigned mid=(min+last)/2;
    //                    while (min <= last) {
    //                        uint64_t slavePtr=slavePtrArr[mid];
    //                        uint64_t slaveSize=slaveSizeArr[mid];
    //
    //                        if ( masterPtr>=slavePtr && masterPtr<=slavePtr+slaveSize)
    //                        {
    //                            //If there is space, mark it as free and stop, if not, just stop and discard this masterPtrv because the space
    //                            //around it is not enough
    //                            spaceHasFreeChunk= spaceHasFreeChunk || (masterPtr>=slavePtr &&
    //                                    masterPtr+chunkSize <= slavePtr+slaveSize && blackList.count(slavePtr)==0);
    //                            break;
    //                        } else if ( slavePtr < masterPtr ) {
    //                            first = mid+1;
    //                        } else {
    //                            last = mid-1;
    //                        }
    //                        mid= (first+last)>>1;
    //                    }
    //                    isAvaiableInAllSpaces= isAvaiableInAllSpaces && spaceHasFreeChunk;
    //                }
    //                if (isAvaiableInAllSpaces) {
    //                    resul=masterPtr;
    //                }
    //            }
    //        }
    //    }
    //    if (resul==0) {
    //        fatal0("Couldn't find any free virtual address common to all nodes when trying to allocate unified memory space");
    //    }
    //    return result;
    return 0;
}

void MPIRemoteNode::DEEP_Booster_free(MPI_Comm *intercomm, int rank) {
    NANOS_MPI_CREATE_IN_MPI_RUNTIME_EVENT(ext::NANOS_MPI_DEEP_BOOSTER_FREE_EVENT);
    cacheOrder order;
    order.opId = OPID_FINISH;
    int nThreads = sys.getNumWorkers();
    //Now sleep the threads which represent the remote processes
    int res = MPI_IDENT;
    bool sharedSpawn = false;

    unsigned int numThreadsWithThisComm = 0;
    std::vector<nanos::ext::MPIThread*> threadsToDespawn;
    std::vector<MPIProcessor*> communicatorsToFree;
    //Find threads and nodes to de-spawn
    for (int i = 0; i < nThreads; ++i) {
        BaseThread* bt = sys.getWorker(i);
        nanos::ext::MPIProcessor * myPE = dynamic_cast<nanos::ext::MPIProcessor *> (bt->runningOn());
        if (myPE && !bt->isSleeping()) {
            MPI_Comm threadcomm = myPE->getCommunicator();
            if (threadcomm != 0 && intercomm != NULL) MPI_Comm_compare(threadcomm, *intercomm, &res);
            if (res == MPI_IDENT) {
                numThreadsWithThisComm++;
                if (myPE->getRank() == rank || rank == -1) {
                    sharedSpawn = sharedSpawn || myPE->getShared();
                    threadsToDespawn.push_back((nanos::ext::MPIThread *)bt);
                    //intercomm NULL (all communicators) and single rank is not supported (and it mostly makes no sense...)
                    //but it will work, except mpi comm free will have to be done by the user after he frees all ranks
                    if (intercomm == NULL && threadcomm != MPI_COMM_NULL && rank == -1) communicatorsToFree.push_back(myPE);
                }
            }
        }
    }
    if (!threadsToDespawn.empty()) {
        //All threads have the same commOfParents as they were spawned together
        MPI_Comm parentsComm = threadsToDespawn.front()->getRunningPEs().at(0)->getCommOfParents();
        int currRank = -1;
        MPI_Comm_rank(parentsComm, &currRank);
        //Synchronize parents before killing shared resources (as each parent only waits for his task
        //this prevents one parent killing a "son" which is still executing things from other parents)
        if (sharedSpawn) {
            MPI_Barrier(parentsComm);
        }
        //De-spawn threads and nodes
        for (std::vector<nanos::ext::MPIThread*>::iterator itThread = threadsToDespawn.begin(); itThread != threadsToDespawn.end(); ++itThread) {
            nanos::ext::MPIThread* mpiThread = *itThread;
            std::vector<MPIProcessor*>& myPEs = mpiThread->getRunningPEs();
            for (std::vector<MPIProcessor*>::iterator it = myPEs.begin(); it != myPEs.end(); ++it) {
                //Only owner will send kill signal to the worker
                if ((*it)->getOwner()) {
                    nanosMPISend(&order, 1, nanos::MPIDevice::cacheStruct, (*it)->getRank(), TAG_M2S_ORDER, *intercomm);
                    //After sending finalization signals, we are not the owners anymore
                    //This way we prevent finalizing them multiple times if more than one thread uses them
                    (*it)->setOwner(false);
                }
                //If im the root do all the automatic control file work ONCE (first remote node), this time free the hosts
                if ((*it)->getRank() == 0 && currRank == 0 && !nanos::ext::MPIProcessor::getMpiControlFile().empty()) {
                    //PPH List is the list of hosts which were consumed by the spawn of these ranks, lets "free" them
                    int* pph_list = (*it)->getPphList();
                    if (pph_list != NULL) {
                        int fdAux = -1;
                        std::string controlName = nanos::ext::MPIProcessor::getMpiControlFile();
                        fdAux = tryGetLock(const_cast<char*> (controlName.c_str()));
                        if (fdAux == -1) fatal0(controlName << " could not be opened/created, check your NX_OFFL_CONTROLFILE environment variable");
                        FILE* fd = fdopen(fdAux, "r+");
                        size_t num_bytes = 0;
                        for (int i = 0; pph_list[i] != -1; ++i) {
                            if (pph_list[i] != 0) {
                                fseek(fd, num_bytes, SEEK_SET);
                                const int freeNode = 0;
                                fprintf(fd, "%d\n", freeNode);
                            }
                            num_bytes += 2;
                        }
                        fclose(fd);
                        delete[] pph_list;
                        (*it)->setPphList(NULL);
                        releaseLock(fdAux, const_cast<char*> (controlName.c_str()));
                    }
                }
            }
            if (rank == -1) {
                mpiThread->lock();
                mpiThread->stop();
                mpiThread->join();
                mpiThread->unlock();
            }
        }
    }
    //If we despawned all the threads which used this communicator, free the communicator
    //If intercomm is null, do not do it, it should be the final free
    if (intercomm != NULL
            //            && threadsToDespawn.size()>=numThreadsWithThisComm
            ) {
        /*
         * Uncomment when nanos offload and user's communicator handles are different
        #ifdef OPEN_MPI
               MPI_Comm_free(intercomm);
        #else
               MPI_Comm_disconnect(intercomm);
        #endif
         */
        *intercomm = MPI_COMM_NULL;
    } else if (communicatorsToFree.size() > 0) {
        for (std::vector<MPIProcessor*>::iterator it = communicatorsToFree.begin(); it != communicatorsToFree.end(); ++it) {
            MPI_Comm comm = (*it)->getCommunicator();
#ifdef OPEN_MPI
            MPI_Comm_free(&comm);
#else
            MPI_Comm_disconnect(&comm);
#endif
            (*it)->setCommunicator(MPI_COMM_NULL);
        }
    }
    NANOS_MPI_CLOSE_IN_MPI_RUNTIME_EVENT;
}

void MPIRemoteNode::DEEPBoosterAlloc(MPI_Comm comm, int number_of_hosts, int process_per_host, MPI_Comm *intercomm, bool strict, int* provided, int offset, int* pph_list) {
    NANOS_MPI_CREATE_IN_MPI_RUNTIME_EVENT(ext::NANOS_MPI_DEEP_BOOSTER_ALLOC_EVENT);
    //Initialize nanos MPI
    nanosMPIInit(0, 0, MPI_THREAD_MULTIPLE, 0);


    if (!MPIDD::getSpawnDone()) {
        int userProvided;
        MPI_Query_thread(&userProvided);
        if (userProvided < MPI_THREAD_MULTIPLE) {
            message0("MPI_Query_Thread returned multithread support less than MPI_THREAD_MULTIPLE, your application may hang when offloading, check your MPI "
                    "implementation and try to configure it so it can support this multithread level. Configure your PATH so the mpi compiler"
                    " points to a multithread implementation of MPI");
            //Some implementations seem to catch fatal0 and continue... make sure we die
            exit(-1);
        }
    }

    std::vector<std::string> tokensParams;
    std::vector<std::string> tokensHost;
    std::vector<int> hostInstances;
    int totalNumberOfSpawns = 0;
    int spawnedHosts = 0;
    int rank;
    MPI_Comm_rank(comm, &rank);

    int availableHosts = 0;
    //Read hostlist
    if (rank == 0 && !nanos::ext::MPIProcessor::getMpiControlFile().empty()) {
        buildHostLists(offset, INT_MAX, tokensParams, tokensHost, hostInstances); //Read all the hosts
        availableHosts = tokensHost.size();
    } else {
        buildHostLists(offset, number_of_hosts, tokensParams, tokensHost, hostInstances);
        availableHosts = tokensHost.size();
        if (availableHosts > number_of_hosts) availableHosts = number_of_hosts;
        //Check strict restrictions and react to them (return or set the right number of nodes)
        if (strict && number_of_hosts > availableHosts) {
            if (provided != NULL) *provided = 0;
            *intercomm = MPI_COMM_NULL;
            return;
        }
    }

    //If im the root do all the automatic control file work
    if (rank == 0 && !nanos::ext::MPIProcessor::getMpiControlFile().empty()) {
        if (offset != 0 || pph_list != NULL) fatal0("NX_OFFL_CONTROLFILE environment variable has been defined, deep_booster_alloc_list is not supported"
                "/needed with automatic control of hosts");
        //Maximum length of pph_list alloc
        pph_list = new int[availableHosts + 1];
        int fdAux = -1;
        int currStatus = 0;
        std::string controlName = nanos::ext::MPIProcessor::getMpiControlFile();
        fdAux = tryGetLock(const_cast<char*> (controlName.c_str()));
        if (fdAux == -1) fatal0(controlName << " could not be opened/created, check your NX_OFFL_CONTROLFILE environment variable");
        FILE* fd = fdopen(fdAux, "r+");
        int reserved = 0;
        //Reserve as many hosts as needed
        int i;
        for (i = 0; i < availableHosts && reserved < number_of_hosts; ++i) {
            long size = ftell(fd);
            int err = fscanf(fd, "%d\n", &currStatus);
            if (currStatus == 0 || err == EOF) {
                fseek(fd, size, SEEK_SET);
                pph_list[i] = process_per_host;
                const int busyNode = 1;
                reserved++;
                fprintf(fd, "%d\n", busyNode);
            } else {
                pph_list[i] = 0;
            }
        }
        fclose(fd);
        releaseLock(fdAux, const_cast<char*> (controlName.c_str()));
        //Mark the real length of pph_list
        availableHosts = i;
        pph_list[availableHosts] = -1; //end of list
        //Check strict restrictions and react to them (return or set the right number of nodes)
        if (strict && number_of_hosts > reserved) {
            if (provided != NULL) *provided = 0;
            *intercomm = MPI_COMM_NULL;
            return;
        }
    }

    //Register spawned processes so nanox can use them
    int mpiSize;
    MPI_Comm_size(comm, &mpiSize);
    bool shared = (mpiSize > 1);

    callMPISpawn(comm, availableHosts, strict, tokensParams, tokensHost, hostInstances, pph_list,
            process_per_host, shared, /* outputs*/ spawnedHosts, totalNumberOfSpawns, intercomm);
    if (provided != NULL) *provided = totalNumberOfSpawns;

    createNanoxStructures(comm, intercomm, spawnedHosts, totalNumberOfSpawns, shared, mpiSize, rank, pph_list);

    NANOS_MPI_CLOSE_IN_MPI_RUNTIME_EVENT;
}

//#ifdef SLURM_IS_USED

void MPIRemoteNode::SLURMbuildHostLists(hostlist_t hl, std::vector<std::string>& tokensParams, std::vector<std::string>& tokensHost, std::vector<int>& hostInstances) {
    std::list<std::string> tmpStorage;
    std::string params = "ompssnoparam";

    char *host = slurm_hostlist_shift(hl);

    while (host != NULL) {
        //printf("[%d]-> %s(%s,%d) || host: %s\n", getpid(), __func__, __FILE__, __LINE__, host);
        //tmpStorage.push_back(line);
        hostInstances.push_back(1);
        tokensHost.push_back(host);
        tokensParams.push_back(params);

        host = slurm_hostlist_shift(hl);
    }
}
//#endif

void MPIRemoteNode::DMRDetachAlloc(void) {
    int world_size, world_rank, name_len;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(processor_name, &name_len);

    /* sockets version*/
    if (_actionRMS == 2) {
        //MPI_Barrier(MPI_COMM_WORLD);
        //printf("(sergio)[%d]: %s(%s,%d) - proc: %s world: %d - current: %d\n", getpid(), __FILE__, __func__, __LINE__, processor_name, world_rank, _currNodes);
        if (world_rank >= _currNodes) {
            pid_t pid = fork();
            if (pid == -1) {
                perror("fork failed");
                exit(EXIT_FAILURE);
            } else if (pid == 0) { //forked processes in the rest of the ranks are clients
                //printf("\t(sergio): %s(%s,%d) - %s %d\n", __FILE__, __func__, __LINE__, processor_name, world_rank);
                int sockfd;
                struct sockaddr_in serv_addr;
                struct hostent *server;

                sockfd = socket(AF_INET, SOCK_STREAM, 0);
                if (sockfd < 0)
                    error("ERROR opening socket");
                server = gethostbyname(_managementHost); //managementHost);
                if (server == NULL) {
                    fprintf(stderr, "ERROR, no such host\n");
                    exit(0);
                }
                bzero((char *) &serv_addr, sizeof (serv_addr));
                //memset(&serv_addr, '\0', sizeof(serv_addr));
                serv_addr.sin_family = AF_INET;
                bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
                serv_addr.sin_port = htons(PORT);
                while (getppid() != 1) {
                    printf("\t(sergio): %s(%s,%d) - %s %d - pid %d, ppid %d\n", __FILE__, __func__, __LINE__, processor_name, world_rank, getpid(), getppid());
                    sleep(1);
                }
                //printf("Before connecting from %s to %s - Socket: %d, size: %lu\n", processor_name, _managementHost, sockfd, sizeof (serv_addr));
                //printf("PID: %d [%d/%d] PPID: %d - Before connecting From %s to %s (on socket %d - %lu len - (fam %i, addr %d, port %i)\n", getpid(), world_rank, world_size, getppid(), processor_name, _managementHost, sockfd, sizeof (serv_addr), serv_addr.sin_family, serv_addr.sin_addr.s_addr, serv_addr.sin_port);
                int newconnect = connect(sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr));
                //printf("PID: %d [%d/%d] PPID: %d - After connecting (return %d) From %s to %s (on socket %d - %lu len - (fam %i, addr %d, port %i)\n", getpid(), world_rank, world_size, getppid(), newconnect, processor_name, _managementHost, sockfd, sizeof (serv_addr), serv_addr.sin_family, serv_addr.sin_addr.s_addr, serv_addr.sin_port);
                if (newconnect < 0) {
                    char output[STR_SIZE];
                    sprintf(output, "PID: %d [%d/%d] ERROR connecting (return %d) From %s to %s (on socket %d - %lu len - (fam %i, addr %d, port %i)", getpid(), world_rank, world_size, newconnect, processor_name, _managementHost, sockfd, sizeof (serv_addr), serv_addr.sin_family, serv_addr.sin_addr.s_addr, serv_addr.sin_port);
                    error(output);
                }
                //printf("After connecting from %s to %s - Socket: %d, size: %lu\n", processor_name, _managementHost, sockfd, sizeof (serv_addr));
                close(sockfd);
                free(_managementHost);
                _exit(0);
            }
        } else if (world_rank == 0) {
            //printf("@@@(sergio)[%d]: %s(%s,%d) - %s %d\n", getpid(), __FILE__, __func__, __LINE__, processor_name, world_rank);
            pthread_join(_socketThread, NULL);
            //printf("(sergio)[%d]: %s(%s,%d) - %s %d JOIN DONE\n", getpid(), __FILE__, __func__, __LINE__, processor_name, world_rank);
            /*
            int n = world_size - _currNodes;
            for (int i = 0; i < n; i++) {
                pthread_join(_pthreads_ids[i], NULL);
            }*/
            //free(_pthreads_ids);
            job_desc_msg_t job_update;
            slurm_init_job_desc_msg(&job_update);

            job_update.job_id = _expandJobId;
            job_update.min_nodes = _currNodes;
            //job_update.req_nodes = _managementHost;
            printf("¿?¿?¿?¿?¿?[%d/%d] Deallocate last %d hosts (expand job %d, min_nodes %d)\n", world_rank, world_size, world_size - _currNodes, job_update.job_id, job_update.min_nodes);
            sleep(10); //auxiliary wait for marenostrum   
            slurm_update_job(&job_update);
            _shrinkFinished = 1;
            //printf("¿?¿?¿?¿?¿?[%d/%d] finish update\n", world_rank, world_size);
        }
        free(_managementHost);
    }

    //while (getNDatasent() < factor)
    //    usleep(10000);

    //setDetached();
    //sleep(10);
    printf("### [%d]-> %s(%s,%d) || Exiting... rank %d/%d from %s\n", getpid(), __func__, __FILE__, __LINE__, world_rank, world_size, processor_name);
    //MPI_Finalize();
    exit(0);
}

struct tokens : std::ctype<char> {

    tokens() : std::ctype<char>(get_table()) {
    }

    static std::ctype_base::mask const* get_table() {
        typedef std::ctype<char> cctype;
        static const cctype::mask *const_rc = cctype::classic_table();

        static cctype::mask rc[cctype::table_size];
        std::memcpy(rc, const_rc, cctype::table_size * sizeof (cctype::mask));

        rc[','] = std::ctype_base::space;
        return &rc[0];
    }
};

//DEPRECATED

void *connection_handler(void * params) {
    struct thread_socket *args = (struct thread_socket*) params;
    //printf("@@@ [%d]-> %s(%s,%d) || Before accepting (%d %s %d)\n", getpid(), __func__, __FILE__, __LINE__, args->sockfd, aux->sa_data, *args->clilen);
    int newsockfd = accept(args->sockfd, (struct sockaddr *) args->cli_addr, (socklen_t *) args->clilen);
    if (newsockfd < 0) {
        error("ERROR on accept");
    }
    //struct sockaddr *aux = (struct sockaddr *) args->cli_addr;
    //printf("(sergio): @@@@@@ Accepted from %s \n", (char *) aux->sa_data);
    //printf("@@@ [%d]-> %s(%s,%d) || After accepting\n", getpid(), __func__, __FILE__, __LINE__);
    //close(newsockfd);
    return 0;
}

void *listener(void * n_deallocs) {
    int world_size, world_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    int sockfd;
    socklen_t clilen;
    struct sockaddr_in serv_addr, cli_addr;
    int n = *((int *) n_deallocs);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("ERROR opening socket");

    int option = 1;
    setsockopt(sockfd, SOL_SOCKET, (SO_REUSEPORT | SO_REUSEADDR), (char*) &option, sizeof (option));
    bzero((char *) &serv_addr, sizeof (serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(PORT);
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0)
        error("ERROR on binding");
    if (listen(sockfd, n) < 0)
        error("ERROR when listening");

    clilen = sizeof (cli_addr);
    //struct thread_socket params = {sockfd, &cli_addr, (int*) &clilen};
    int cnt = 0;
    while (cnt < n) {
        //printf("(sergio): %s(%s,%d) - start accept %d/%d on socket %d (%i len - (fam %i, addr %i, port %i)\n", __FILE__, __func__, __LINE__, cnt + 1, n, sockfd, clilen, serv_addr.sin_family, serv_addr.sin_addr.s_addr, serv_addr.sin_port);
        int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
        //printf("(sergio): %s(%s,%d) - end accept (return %d) %d/%d on socket %d (%i len - (fam %i, addr %i, port %i)\n", __FILE__, __func__, __LINE__, newsockfd, cnt + 1, n, sockfd, clilen, serv_addr.sin_family, serv_addr.sin_addr.s_addr, serv_addr.sin_port);
        if (newsockfd < 0) {
            char output[STR_SIZE];
            sprintf(output, "PID: %d [%d/%d] ERROR accepting (return %d)", getpid(), world_rank, world_size, newsockfd);
            error(output);
        }
        cnt++;
    }
    //_pthreads_ids = (pthread_t *) malloc(sizeof (pthread_t) * n);

    /*
        while (cnt < n) {
            //printf("(sergio): [%d/%d](%s) Thread: %d - \n", world_rank, world_size, processor_name, cnt);
            if (pthread_create(&_pthreads_ids[cnt], NULL, connection_handler, (void*) &params) < 0)
                error("could not create thread");
            //printf("@@@ Created thread %d id %lu\n", cnt, _pthreads_ids[cnt]);
            cnt++;
        }
        //printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);
     */

    //¿?¿?¿?¿?¿?¿?¿?¿?¿?¿?¿?¿?¿?¿?¿?¿?¿?¿?¿?¿?¿?¿?¿?
    //close(sockfd);
    return 0;
}

int MPIRemoteNode::DMRCheckStatus(int min, int max, int step, int pref, int *nnodes, MPI_Comm *intercomm) {
    int world_size, world_rank, name_len;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(processor_name, &name_len);
    //char *host = NULL;
    int counter = 0;
    bool check = false;

    //printf("(sergio): [%d/%d](%s)\n", world_rank, world_size, processor_name);

    if (world_rank == 0)
        check = checkTimePeriod();
    //printf("(sergio): %s(%s,%d) -> Aún no se ha superado el periodo de planificación de cada %d segundos. Faltan %d segundos\n", __FILE__, __func__, __LINE__, getPeriod(), getPeriod() - (int) (wall_time() - getWalltime()));
    MPI_Bcast(&check, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (!check)
        return -2;

    if (world_rank == 0) {
        char *pID = getenv("SLURM_JOBID");
        uint32_t procID = atoi(pID);
        _expandJobId = (int) procID;
        job_info_msg_t *MasterInfo;
        slurm_load_job(&MasterInfo, procID, 0);
        *nnodes = 0;
        //Waiting for stabilisation version without sincronous sockets
        while (world_size != (int) MasterInfo->job_array->num_nodes) {
            printf("waiting for stabilization... (current %d - slurm %d)\n", world_size, MasterInfo->job_array->num_nodes);
            sleep(5);
            slurm_free_job_info_msg(MasterInfo);
            slurm_load_job(&MasterInfo, procID, 0);
        }
        //Slurm chooses the action in the selection policy
        struct select_jobinfo *jobinfo = (struct select_jobinfo *) malloc(sizeof (struct select_jobinfo));
        MasterInfo->job_array->select_jobinfo->data = jobinfo;
        jobinfo->job_id = (uint32_t) procID;
        jobinfo->min = (uint32_t) min;
        jobinfo->max = (uint32_t) max;
        jobinfo->preference = (uint32_t) pref;
        jobinfo->step = (uint32_t) step;
        jobinfo->currentNodes = (uint32_t) world_size;
        slurm_get_select_jobinfo(MasterInfo->job_array->select_jobinfo, SELECT_JOBDATA_ACTION, NULL);

        _actionRMS = jobinfo->action;
        *nnodes = jobinfo->resultantNodes;

        printf("[%d]-> %s(%s,%d) || Action: %d %d - Current nodes %d, while in slurm %d\n", getpid(), __func__, __FILE__, __LINE__, jobinfo->action, jobinfo->resultantNodes, world_size, MasterInfo->job_array->num_nodes);
        //free(jobinfo);

        if (_actionRMS == 2) {
            //Decidimos que nodo será el que gestione la sincronización por sockets.
            //Para ello llamamos a la API y así obtener el número de nodos del principio del jobnodelist que no van a ser liberados
            //Esta solución, responde a necesidades semánticas. Aunque de todas formas sabemos que será el primer nodo el elegido.
            ////
            //FALTA ARREGLAR la llamada a slurm (podría ser más fácil hacer un slurm_load_job dentro de linear.cpp para coneguir los nodos)
            ////
            //strcpy(jobinfo->hostlist, MasterInfo->job_array->nodes);
            //_managementHost = (char *) malloc(sizeof (char) * HOSTLEN);
            //slurm_get_select_jobinfo(MasterInfo->job_array->select_jobinfo, SELECT_JOBDATA_INFO, (void *) _managementHost);
            _managementHost = (char *) malloc(sizeof (char) * MPI_MAX_PROCESSOR_NAME);
            strcpy(_managementHost, processor_name);
            //printf("(sergio): %s(%s,%d) - %s management host %s\n", __FILE__, __func__, __LINE__, jobinfo->hostlist, _managementHost);
        }

        free(jobinfo);
        slurm_free_job_info_msg(MasterInfo);
    }

    if (world_rank == 0) {
        //printf("(sergio): %s(%s,%d) - action %d to end with %d nodes\n", __FILE__, __func__, __LINE__, _actionRMS, *nnodes);
        switch (_actionRMS) {
            case 0:
                break;

            case 1: //expand
                job_desc_msg_t job_desc;
                job_info_msg_t *jobAux;
                resource_allocation_response_msg_t* slurm_alloc_msg_ptr;
                slurm_init_job_desc_msg(&job_desc);
                job_desc.name = (char *) "resizer";
                job_desc.dependency = (char *) malloc(sizeof (char)*64);
                sprintf(job_desc.dependency, (char *) "expand:%d", _expandJobId);
                job_desc.user_id = getuid();
                job_desc.priority = 1000000;
                job_desc.min_nodes = *nnodes - world_size;

                if (slurm_allocate_resources(&job_desc, &slurm_alloc_msg_ptr)) {
                    slurm_perror((char *) "slurm_allocate_resources error (DMRCheckStatus)");
                    exit(1);
                }
                free(job_desc.dependency);

                _expandJobId = slurm_alloc_msg_ptr->job_id;
                if (slurm_alloc_msg_ptr->node_list == NULL) {
                    slurm_load_job(&jobAux, _expandJobId, 0);
                    //printf("(sergio): %s(%s,%d) - %d %s\n", __FILE__, __func__, __LINE__, _expandJobId, jobAux->job_array->nodes);
                    while (jobAux->job_array->nodes == NULL) {
                        slurm_free_job_info_msg(jobAux);
                        printf("Waiting for backfilling (check sinfo or priorities)\n");
                        counter++;
                        sleep(5);
                        if (counter == 10) {
                            printf("Backfilling not completed\n");
                            _actionRMS = 0;
                            slurm_kill_job(_expandJobId, SIGKILL, 0);
                            slurm_free_resource_allocation_response_msg(slurm_alloc_msg_ptr);
                            _expandJobId = -1;
                            break;
                        }
                        slurm_load_job(&jobAux, _expandJobId, 0);
                    }
                    if (_actionRMS != 0)
                        slurm_free_job_info_msg(jobAux);
                }
                break;

            case 2: //shrink
                int n = world_size - *nnodes;
                //_pthreads_ids = (pthread_t *) malloc(sizeof (pthread_t));
                if (pthread_create(&_socketThread, NULL, listener, (void *) &n) < 0)
                    error("could not create thread");
                break;
        }
    }
    MPI_Bcast(&_actionRMS, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(nnodes, 1, MPI_INT, 0, MPI_COMM_WORLD);
    _currNodes = *nnodes;
    if (_actionRMS == 2) {
        if (world_rank > 0)
            _managementHost = (char *) malloc(sizeof (char) * MPI_MAX_PROCESSOR_NAME);
        MPI_Bcast(_managementHost, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, 0, MPI_COMM_WORLD);
    }

    switch (_actionRMS) {
        case 0:
            break;

        case 1: //expand
            DMRExpandJob(MPI_COMM_WORLD, _currNodes, 1, intercomm, true, NULL, 0, NULL);
            break;

        case 2: //shrink
            DMRShrinkJob(MPI_COMM_WORLD, _currNodes, 1, intercomm, true, NULL, 0, NULL);

            break;
    }

    return _actionRMS;
}

void * async_sched(void * params) {
    //sleep(5);
    printf("(sergio): %s(%s,%d) THREAD CREATED!!!\n", __FILE__, __func__, __LINE__);
    job_info_msg_t *MasterInfo;
    struct thread_sched *args = (struct thread_sched *) params;
    while (state) {
        //pthread_mutex_lock(&mutex);
        waiting = true;
        pthread_cond_wait(&cond, &mutex);
        waiting = false;

        if (args->ptrShrinkFinished[0] == 0) {
            //            args->ptrCurrNodes[0] = 0;
            //args->ptrActionRMS[0] = 0;
            return 0;
        }

        //pthread_mutex_unlock(&mutex);
        //printf("(sergio): %s(%s,%d) -> %d %d %d %d %d\n", __FILE__, __func__, __LINE__, args->job_id, args->min[0], args->max[0], args->step[0], args->currentNodes[0]);
        struct select_jobinfo *jobinfo = (struct select_jobinfo *) malloc(sizeof (struct select_jobinfo));
        jobinfo->job_id = (uint32_t) args->job_id;
        jobinfo->min = (uint32_t) args->min[0];
        jobinfo->max = (uint32_t) args->max[0];
        jobinfo->preference = (uint32_t) args->preference[0];
        jobinfo->step = (uint32_t) args->step[0];
        jobinfo->currentNodes = (uint32_t) args->currentNodes[0];

        slurm_load_job(&MasterInfo, jobinfo->job_id, 0);
        MasterInfo->job_array->select_jobinfo->data = jobinfo;
        slurm_get_select_jobinfo(MasterInfo->job_array->select_jobinfo, SELECT_JOBDATA_ACTION, NULL);
        if (jobinfo->action == 2) {
            char *host;
            hostlist_t hl = slurm_hostlist_create(MasterInfo->job_array->nodes);
            host = slurm_hostlist_shift(hl);
            strcpy(args->ptrManagementNode, host);
            free(host);
            slurm_hostlist_destroy(hl);
        }

        args->ptrCurrNodes[0] = jobinfo->resultantNodes;
        args->ptrActionRMS[0] = jobinfo->action;
        if (args->ptrActionRMS[0] == 2)
            args->ptrShrinkFinished[0] = 0;

        //printf("(sergio): %s(%s,%d) -> %d %s\n", __FILE__, __func__, __LINE__, jobinfo->action, args->ptrManagementNode);
        free(jobinfo);
        slurm_free_job_info_msg(MasterInfo);
    }
    return 0;
}

int MPIRemoteNode::DMRICheckStatus(int min, int max, int step, int pref, int *nnodes, MPI_Comm * intercomm) {
    int world_size, world_rank, name_len;
    int counter = 0, check = 0, aux = 0;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(processor_name, &name_len);

    //Waiting for stabilisation version without sincronous sockets
    if (world_rank == 0) {
        char *jobid = getenv("SLURM_JOBID");
        uint32_t procID = atoi(jobid);
        job_info_msg_t *MasterInfo;
        slurm_load_job(&MasterInfo, procID, 0);
        aux = (int) MasterInfo->job_array->num_nodes;
        check = world_size == aux;
        slurm_free_job_info_msg(MasterInfo);
    }
    MPI_Bcast(&check, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (!check) {
        if (world_rank == 0)
            printf("There is no scheduling (check = %d) this step, waiting for stabilization... (current %d - slurm %d)\n", check, world_size, aux);
        return -2;
    }

    check = false;
    if (world_rank == 0)
        check = checkTimePeriod();
    //printf("(sergio): %s(%s,%d) -> Aún no se ha superado el periodo de planificación de cada %d segundos. Faltan %d segundos\n", __FILE__, __func__, __LINE__, getPeriod(), getPeriod() - (int) (wall_time() - getWalltime()));
    MPI_Bcast(&check, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (!check)
        return -2;

    if (world_rank == 0)
        if (_currNodes > -1) {
            //printf("(sergio): %s(%s,%d) -> He terminado la planificación: action %d nodes %d\n", __FILE__, __func__, __LINE__, _actionRMS, _currNodes);
            printf("(sergio): %s(%s,%d) -> He terminado la planificación: action %d nodes %d  (finish %d)\n", __FILE__, __func__, __LINE__, _actionRMS, _currNodes, _shrinkFinished);
            if (_actionRMS > 0) {
                //va a ver un spawn, termina el thread en este comm.
                state = false;
                //pthread_mutex_lock(&mutex);
                while (waiting)
                    pthread_cond_signal(&cond);
                //pthread_mutex_unlock(&mutex);
            }
        }

    MPI_Bcast(&_actionRMS, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&_currNodes, 1, MPI_INT, 0, MPI_COMM_WORLD);
    *nnodes = _currNodes;

    switch (_actionRMS) {
        case -1: //as long as the this is the first step of this communicator, the var action will be equal to -1
            if (world_rank == 0) {
                //printf("(sergio): %s(%s,%d) -> %d\n", __FILE__, __func__, __LINE__, _expandJobId);
                //thread creation
                //char *pID = getenv("SLURM_JOBID");
                //int procID = atoi(pID);
                //_expandJobId = procID;
                //_currNodes = -1;

                _managementHost = (char *) malloc(sizeof (char) * HOSTLEN);
                ////////////////////////////////////////////////////////////////
                //ACTUALIZAR a la llamada API slurm para obtener el host
                ////////////////////////////////////////////////////////////////

                struct thread_sched *schedParams = (struct thread_sched *) malloc(sizeof (struct thread_sched));
                schedParams->job_id = _expandJobId; //initially is initialized to the slurm  jobID, then it is changed
                schedParams->min = &gmin;
                schedParams->max = &gmax;
                schedParams->preference = &gpref;
                schedParams->step = &gstep;
                schedParams->currentNodes = &gnodes;
                schedParams->ptrCurrNodes = &_currNodes;
                schedParams->ptrActionRMS = &_actionRMS;
                schedParams->ptrExpandJob = &_expandJobId;
                schedParams->ptrManagementNode = _managementHost;
                schedParams->ptrShrinkFinished = &_shrinkFinished;

                state = true;
                pthread_t asyncThread;
                if (pthread_create(&asyncThread, NULL, async_sched, (void*) schedParams) < 0)
                    error("could not create thread");

            }
            _actionRMS = -2;
        case -2: //actionRMS -2 indica que se tiene que planificar en la próxima iteración.
            //printf("(sergio): %s(%s,%d) action %d  - nodes %d\n", __FILE__, __func__, __LINE__, _actionRMS, _currNodes);
            _actionRMS = 0;
            if (world_rank == 0) {
                printf("(sergio): %s(%s,%d) action %d  - nodes %d\n", __FILE__, __func__, __LINE__, _actionRMS, _currNodes);
                //wake up thread and schedule action
                //printf("(sergio): %s(%s,%d) -> %d %d %d %d\n", __FILE__, __func__, __LINE__, min, max, step, world_size);
                //sleep(5);
                gmin = min;
                gmax = max;
                gpref = pref;
                gstep = step;
                gnodes = world_size;
                printf("(sergio): %s(%s,%d) action %d  - nodes %d - waiting %d\n", __FILE__, __func__, __LINE__, _actionRMS, _currNodes, waiting);
                while (waiting) {
                    pthread_cond_signal(&cond);
                }
            }
            break;

        case 0: //no action
            //if action has been taken (_currNodes > -1), schedule again (_actionRMS == -2)
            if (world_rank == 0)
                printf("(sergio): %s(%s,%d) action %d  - nodes %d\n", __FILE__, __func__, __LINE__, _actionRMS, _currNodes);
            if (_currNodes > -1) {
                _actionRMS = -2;
                _currNodes = -1;
            }
            break;

        case 1: //expand
            if (world_rank == 0) {
                //printf("(sergio): %s(%s,%d) -> %d %d %d %d\n", __FILE__, __func__, __LINE__, _actionRMS, _expandJobId, *nnodes, world_size);
                job_desc_msg_t job_desc;
                job_info_msg_t *jobAux;
                resource_allocation_response_msg_t* slurm_alloc_msg_ptr;
                slurm_init_job_desc_msg(&job_desc);
                char *pID = getenv("SLURM_JOBID");
                job_desc.name = (char *) malloc(sizeof (char) * 20);
                //sprintf(job_desc.name, (char *) "resizer-%s", pID);
                sprintf(job_desc.name, (char *) "resizer");
                job_desc.dependency = (char *) malloc(sizeof (char)*20);
                sprintf(job_desc.dependency, (char *) "expand:%d", _expandJobId);
                job_desc.user_id = getuid();
                job_desc.priority = 1000000;
                job_desc.min_nodes = *nnodes - world_size;

                if (slurm_allocate_resources(&job_desc, &slurm_alloc_msg_ptr)) {
                    slurm_perror((char *) "slurm_allocate_resources error (DMRCheckStatus)");
                    exit(1);
                }
                free(job_desc.dependency);
                free(job_desc.name);

                _expandJobId = slurm_alloc_msg_ptr->job_id;
                if (slurm_alloc_msg_ptr->node_list == NULL) {
                    slurm_load_job(&jobAux, _expandJobId, 0);
                    //printf("(sergio): %s(%s,%d) - %d %s\n", __FILE__, __func__, __LINE__, _expandJobId, jobAux->job_array->nodes);
                    while (jobAux->job_array->nodes == NULL) {
                        printf("Waiting for backfilling (check sinfo or priorities)\n");
                        counter++;
                        sleep(10);
                        if (counter == 5) {
                            printf("Backfilling not completed\n");
                            slurm_kill_job(_expandJobId, SIGKILL, 0);
                            _actionRMS = -1; // after action=1 the thread will die, we must invoke another.
                            _expandJobId = atoi(pID); //it is needed by async_sched())
                            break;
                        }
                        slurm_free_job_info_msg(jobAux);
                        slurm_load_job(&jobAux, _expandJobId, 0);
                    }
                    slurm_free_job_info_msg(jobAux);
                }
                slurm_free_resource_allocation_response_msg(slurm_alloc_msg_ptr);
            }

            MPI_Bcast(&_actionRMS, 1, MPI_INT, 0, MPI_COMM_WORLD);
            if (_actionRMS != -1)
                DMRExpandJob(MPI_COMM_WORLD, _currNodes, 1, intercomm, true, NULL, 0, NULL);
            break;

        case 2: //shrink

            ////////////////////////////////////////////////////////////////
            //ACTUALIZAR al nuevo esquema de sockets
            ////////////////////////////////////////////////////////////////

            if (world_rank > 0)
                _managementHost = (char *) malloc(sizeof (char) * HOSTLEN);
            //else
            //    printf("(sergio): %s(%s,%d) -> %d %s\n", __FILE__, __func__, __LINE__, _actionRMS, _managementHost);
            MPI_Bcast(_managementHost, HOSTLEN, MPI_CHAR, 0, MPI_COMM_WORLD);
            if (world_rank == 0) {
                int n = world_size - *nnodes;
                //_pthreads_ids = (pthread_t *) malloc(sizeof (pthread_t));
                if (pthread_create(&_socketThread, NULL, listener, (void *) &n) < 0)
                    error("could not create thread");

                /*
                int sockfd;
                socklen_t clilen;
                //char buffer[256];
                struct sockaddr_in serv_addr, cli_addr;
                int n;

                sockfd = socket(AF_INET, SOCK_STREAM, 0);
                if (sockfd < 0)
                    error("ERROR opening socket");

                sockfd = socket(AF_INET, SOCK_STREAM, 0);
                int option = 1;
                setsockopt(sockfd, SOL_SOCKET, (SO_REUSEPORT | SO_REUSEADDR), (char*) &option, sizeof (option));
                bzero((char *) &serv_addr, sizeof (serv_addr));
                serv_addr.sin_family = AF_INET;
                serv_addr.sin_addr.s_addr = INADDR_ANY;
                serv_addr.sin_port = htons(PORT);
                if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0)
                    error("ERROR on binding");

                listen(sockfd, 5);
                clilen = sizeof (cli_addr);
                struct thread_socket params = {sockfd, &cli_addr, (int*) &clilen};
                int cnt = 0;
                n = world_size - 1;
                n = world_size - _currNodes;
                _pthreads_ids = (pthread_t *) malloc(sizeof (pthread_t) * n);
                while (cnt < n) {
                    if (pthread_create(&_pthreads_ids[cnt], NULL, connection_handler, (void*) &params) < 0)
                        error("could not create thread");
                    cnt++;
                }*/

            }
            DMRShrinkJob(MPI_COMM_WORLD, _currNodes, 1, intercomm, true, NULL, 0, NULL);

            break;
    }
    return _actionRMS;
}

int MPIRemoteNode::DMRShrinkJob(MPI_Comm comm, int number_of_hosts, int process_per_host,
        MPI_Comm *intercomm, bool strict, int* provided, int offset, int* pph_list) {
    //NANOS_MPI_CREATE_IN_MPI_RUNTIME_EVENT(ext::NANOS_MPI_DEEP_BOOSTER_ALLOC_EVENT);
    //Initialize nanos MPI
    nanosMPIInit(0, 0, MPI_THREAD_MULTIPLE, 0);

    if (!MPIDD::getSpawnDone()) {
        int userProvided;
        MPI_Query_thread(&userProvided);
        if (userProvided < MPI_THREAD_MULTIPLE) {
            message0("MPI_Query_Thread returned multithread support less than MPI_THREAD_MULTIPLE, your application may hang when offloading, check your MPI "
                    "implementation and try to configure it so it can support this multithread level. Configure your PATH so the mpi compiler"
                    " points to a multithread implementation of MPI");
            //Some implementations seem to catch fatal0 and continue... make sure we die
            exit(-1);
        }
    }

    std::vector<std::string> tokensParams;
    std::vector<std::string> tokensHost;
    std::vector<int> hostInstances;
    int totalNumberOfSpawns = 0;
    int spawnedHosts = 0;
    int world_size, world_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    std::string tokensHostString; //, tokensParamsString;
    char *currHostnames = (char *) malloc(sizeof (char) * STR_SIZE);
    //char *finalNodelist = (char *) malloc(sizeof (char) * STR_SIZE);

    //printf("@@@[%d/%d]:[%d]-> %s(%s,%d)\n", rank, world_size, getpid(), __func__, __FILE__, __LINE__);
    _actionRMS = 0;

    job_info_msg_t *MasterInfo;
    char *pID = getenv("SLURM_JOBID");
    uint32_t procID = atoi(pID);

    if (world_rank == 0) {
        slurm_load_job(&MasterInfo, procID, 0);
        if (MasterInfo == NULL || MasterInfo->record_count == 0) {
            slurm_perror((char *) "%%%%%% No info %%%%%%");
            exit(1);
        }

        readableHostlistTruncated(MasterInfo->job_array[0].nodes, currHostnames, number_of_hosts);
        //printf("[%d]-> %s(%s,%d) || %s - %s\n", getpid(), __func__, __FILE__, __LINE__, currHostnames, MasterInfo->job_array[0].nodes);
        slurm_free_job_info_msg(MasterInfo);
    }

    MPI_Bcast(currHostnames, STR_SIZE + 1, MPI_CHAR, 0, comm);

    hostlist_t hl = slurm_hostlist_create(currHostnames);
    SLURMbuildHostLists(hl, tokensParams, tokensHost, hostInstances);
    slurm_hostlist_destroy(hl);

    //_currNodenames = (char *) malloc(sizeof (char) * strlen(currHostnames));
    //strcpy(_currNodenames, currHostnames);
    free(currHostnames);
    _currNodes = hostInstances.size();

    //Register spawned processes so nanox can use them
    int mpiSize;
    MPI_Comm_size(comm, &mpiSize);
    bool shared = (mpiSize > 1);
    //sleep(2);
    //printf("[%d/%d]:[%d]-> %s(%s,%d) || from %d nodes to %d (%s)\n", world_rank, world_size, getpid(), __func__, __FILE__, __LINE__, mpiSize, (int) hostInstances.size(), _currNodenames);
    callMPISpawn(comm, _currNodes, strict, tokensParams, tokensHost, hostInstances, pph_list,
            process_per_host, shared, /* outputs*/ spawnedHosts, totalNumberOfSpawns, intercomm);

    if (provided != NULL) *provided = totalNumberOfSpawns;
    createNanoxStructures(comm, intercomm, spawnedHosts, totalNumberOfSpawns, shared, mpiSize, world_rank, pph_list);

    _actionRMS = 2;

    return 0;
    //NANOS_MPI_CLOSE_IN_MPI_RUNTIME_EVENT;
}

int MPIRemoteNode::DMRExpandJob(MPI_Comm comm, int number_of_hosts, int process_per_host,
        MPI_Comm *intercomm, bool strict, int* provided, int offset, int* pph_list) {
    //NANOS_MPI_CREATE_IN_MPI_RUNTIME_EVENT(ext::NANOS_MPI_DEEP_BOOSTER_ALLOC_EVENT);
    //Initialize nanos MPI
    nanosMPIInit(0, 0, MPI_THREAD_MULTIPLE, 0);

    if (!MPIDD::getSpawnDone()) {
        int userProvided;
        MPI_Query_thread(&userProvided);
        if (userProvided < MPI_THREAD_MULTIPLE) {
            message0("MPI_Query_Thread returned multithread support less than MPI_THREAD_MULTIPLE, your application may hang when offloading, check your MPI "
                    "implementation and try to configure it so it can support this multithread level. Configure your PATH so the mpi compiler"
                    " points to a multithread implementation of MPI");
            //Some implementations seem to catch fatal0 and continue... make sure we die
            exit(-1);
        }
    }

    std::vector<std::string> tokensParams;
    std::vector<std::string> tokensHost;
    std::vector<int> hostInstances;
    int totalNumberOfSpawns = 0;
    int spawnedHosts = 0;
    int world_size, world_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    std::string tokensHostString; //, tokensParamsString;
    char *finalNodelist = (char *) malloc(sizeof (char) * STR_SIZE);
    //printf("@@@[%d]-> %s(%s,%d)\n", getpid(), __func__, __FILE__, __LINE__);

    _actionRMS = 0;

    job_desc_msg_t job_update;
    job_info_msg_t *MasterInfo, *ExpandInfo;
    char *jID = getenv("SLURM_JOBID");
    uint32_t jobID = atoi(jID);

    if (world_rank == 0) {
        slurm_load_job(&MasterInfo, jobID, 0);
        if (MasterInfo == NULL || MasterInfo->record_count == 0) {
            slurm_perror((char *) "%%%%%% No info %%%%%%");
            exit(1);
        }

        slurm_load_job(&ExpandInfo, _expandJobId, 0);
        if (ExpandInfo == NULL || ExpandInfo->record_count == 0) {
            slurm_perror((char *) "%%%%%% No info %%%%%%");
            exit(1);
        }

        char currHostnames[2048], appendHostnames[2048];
        readableHostlist(MasterInfo->job_array[0].nodes, currHostnames);
        readableHostlist(ExpandInfo->job_array[0].nodes, appendHostnames);
        sprintf(finalNodelist, "%s,%s", currHostnames, appendHostnames);
        //printf("[%d]-> %s(%s,%d) || %s - %s - %s\n", getpid(), __func__, __FILE__, __LINE__, currHostnames, appendHostnames, finalNodelist);
        slurm_free_job_info_msg(ExpandInfo);
        slurm_free_job_info_msg(MasterInfo);

        //$ scontrol update jobid=$SLURM_JOBID NumNodes=0
        slurm_init_job_desc_msg(&job_update);
        job_update.job_id = _expandJobId;
        job_update.min_nodes = 0;
        if (slurm_update_job(&job_update)) {
            slurm_perror((char *) "slurm_update_job error");
            exit(1);
        }
        //sleep(2);
        //exit
        if (slurm_kill_job(_expandJobId, SIGKILL, 0)) {
            slurm_perror((char *) "slurm_kill_job error");
            exit(1);
        }
        //sleep(2);
        //$ scontrol update jobid=$SLURM_JOBID NumNodes=ALL
        //printf("[%d]-> %s(%s,%d) || %d\n", getpid(), __func__, __FILE__, __LINE__, job.min_nodes);
        slurm_init_job_desc_msg(&job_update);
        job_update.job_id = jobID;
        job_update.min_nodes = INFINITE;
        if (slurm_update_job(&job_update)) {
            slurm_perror((char *) "slurm_update_job error");
            exit(1);
        }
        //sleep(2);
    }

    MPI_Bcast(finalNodelist, STR_SIZE + 1, MPI_CHAR, 0, comm);

    hostlist_t hl = slurm_hostlist_create(finalNodelist);
    SLURMbuildHostLists(hl, tokensParams, tokensHost, hostInstances);
    slurm_hostlist_destroy(hl);

    bool shared = (world_size > 1);
    //sleep(2);
    //printf("[%d/%d]:[%d]-> %s(%s,%d) || Nodes: %d (%s)\n", world_rank, world_size, getpid(), __func__, __FILE__, __LINE__, (int) hostInstances.size(), finalNodelist);
    free(finalNodelist);
    callMPISpawn(comm, hostInstances.size(), strict, tokensParams, tokensHost, hostInstances, pph_list,
            process_per_host, shared, spawnedHosts, totalNumberOfSpawns, intercomm);

    if (provided != NULL) *provided = totalNumberOfSpawns;
    createNanoxStructures(comm, intercomm, spawnedHosts, totalNumberOfSpawns, shared, world_size, world_rank, pph_list);

    _actionRMS = 1; //> 0 indica que va a haber un spawn

    return 0;
    //NANOS_MPI_CLOSE_IN_MPI_RUNTIME_EVENT;
}

static inline void trim(std::string& params) {
    //Trim params
    size_t pos = params.find_last_not_of(" \t");
    if (std::string::npos != pos) params = params.substr(0, pos + 1);
    pos = params.find_first_not_of(" \t");
    if (std::string::npos != pos) params = params.substr(pos);
}

void MPIRemoteNode::buildHostLists(
        int offset,
        int requestedHostNum,
        std::vector<std::string>& tokensParams,
        std::vector<std::string>& tokensHost,
        std::vector<int>& hostInstances) {
    std::string mpiHosts = nanos::ext::MPIProcessor::getMpiHosts();
    std::string mpiHostsFile = nanos::ext::MPIProcessor::getMpiHostsFile();
    /** Build current host list */
    std::list<std::string> tmpStorage;
    //In case a host has no parameters, we'll fill our structure with this one
    std::string params = "ompssnoparam";
    //Store single-line env value or hostfile into vector, separated by ';' or '\n'
    if (!mpiHosts.empty()) {
        std::stringstream hostInput(mpiHosts);
        std::string line;
        while (getline(hostInput, line, ';')) {
            if (offset > 0) offset--;
            else tmpStorage.push_back(line);
        }
    } else if (!mpiHostsFile.empty()) {
        std::ifstream infile(mpiHostsFile.c_str());
        fatal_cond0(infile.bad(), "DEEP_Booster alloc error, NX_OFFL_HOSTFILE file not found");
        std::string line;
        while (getline(infile, line, '\n')) {
            if (offset > 0) offset--;
            else tmpStorage.push_back(line);
        }
        infile.close();
    }

    while (!tmpStorage.empty() && (int) tokensHost.size() < requestedHostNum) {
        std::string line = tmpStorage.front();
        tmpStorage.pop_front();
        //If not commented add it to hosts
        if (!line.empty() && line.find("#") != 0) {
            size_t posSep = line.find(":");
            size_t posEnd = line.find("<");
            if (posEnd == line.npos) {
                posEnd = line.size();
            } else {
                params = line.substr(posEnd + 1, line.size());
                trim(params);
            }
            if (posSep != line.npos) {
                std::string realHost = line.substr(0, posSep);
                int number = atoi(line.substr(posSep + 1, posEnd).c_str());
                trim(realHost);
                //Hosts with 0 instances in the file are ignored
                if (!realHost.empty() && number != 0) {
                    hostInstances.push_back(number);
                    tokensHost.push_back(realHost);
                    tokensParams.push_back(params);
                }
            } else {
                std::string realHost = line.substr(0, posEnd);
                trim(realHost);
                if (!realHost.empty()) {
                    hostInstances.push_back(1);
                    tokensHost.push_back(realHost);
                    tokensParams.push_back(params);
                }
            }
        }
    }
    bool emptyHosts = tokensHost.empty();
    //If there are no hosts, that means user "wants" to spawn in localhost
    while (emptyHosts && (int) tokensHost.size() < requestedHostNum) {
        tokensHost.push_back("localhost");
        tokensParams.push_back(params);
        hostInstances.push_back(1);
    }
    if (emptyHosts) {
        warning0("No hostfile or list was providen using NX_OFFL_HOSTFILE or NX_OFFL_HOSTS environment variables."
                " Deep_booster_alloc allocation will be performed in localhost (not recommended except for debugging)");
    }
}

void MPIRemoteNode::callMPISpawn(
        MPI_Comm comm,
        const int availableHosts,
        const bool strict,
        std::vector<std::string>& tokensParams,
        std::vector<std::string>& tokensHost,
        std::vector<int>& hostInstances,
        const int* pph_list,
        const int process_per_host,
        const bool& shared,
        int& spawnedHosts,
        int& totalNumberOfSpawns,
        MPI_Comm* intercomm) {
    std::string mpiExecFile = nanos::ext::MPIProcessor::getMpiExecFile();
    std::string _mpiLauncherFile = nanos::ext::MPIProcessor::getMpiLauncherFile();
    bool pphFromHostfile = process_per_host <= 0;
    bool usePPHList = pph_list != NULL;
    // Spawn the remote process using previously parsed parameters
    std::string result_str;
    if (!mpiExecFile.empty()) {
        result_str = mpiExecFile;
    } else {
        char result[ PATH_MAX ];
        ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
        std::string result_tmp(result, count);
        fatal_cond0(count == 0, "Couldn't identify executable filename, please specify it manually using NX_OFFL_EXEC environment variable");
        result_str = result_tmp.substr(0, count);
    }

    /** Build spawn structures */
    //Number of spawns = max length (one instance per host)
    std::vector<char **> argvs(availableHosts);
    std::vector<char *> commands(availableHosts, &_mpiLauncherFile[0]); // Warning: This may not work
    std::vector<mpi::HostInfo> host_info(availableHosts);
    std::vector<int> num_processes(availableHosts, 0);

    spawnedHosts = 0;
    //This the real length of previously declared arrays, it will be equal to number_of_spawns when
    //hostfile/line only has one instance per host (aka no host:nInstances)
    for (int hostCounter = 0; hostCounter < availableHosts; hostCounter++) {
        //Fill host
        std::string host;
        //Set number of instances this host can handle (depends if user specified, hostList specified or list specified)
        int currHostInstances;
        if (usePPHList) {
            currHostInstances = pph_list[hostCounter];
        } else if (pphFromHostfile) {
            currHostInstances = hostInstances.at(hostCounter);
        } else {
            currHostInstances = process_per_host;
        }
        if (currHostInstances != 0) {
            host = tokensHost.at(hostCounter);
            //If host is a file, give it to Intel, otherwise put the host in the spawn
            std::ifstream hostfile(host.c_str());
            bool isfile = hostfile;
            if (isfile) {
                std::string line;
                int number_of_lines_in_file = 0;
                while (std::getline(hostfile, line)) {
                    ++number_of_lines_in_file;
                }

                host_info[hostCounter].set("hostfile", host);
                currHostInstances = number_of_lines_in_file*currHostInstances;
            } else {
                host_info[hostCounter].set("host", host);
            }
            //In case the MPI implementation supports soft key...
            if (!strict) {
                host_info[hostCounter].set("soft", "0:N");
            }

            hostfile.close();

            //Fill parameter array (including env vars)
            std::stringstream allParamTmp(tokensParams.at(hostCounter));
            std::string tmpParam;
            int paramsSize = 3;
            while (getline(allParamTmp, tmpParam, ',')) {
                paramsSize++;
            }
            std::stringstream all_param(tokensParams.at(hostCounter));
            char **argvv = new char*[paramsSize];
            //Fill the params
            argvv[0] = const_cast<char*> (result_str.c_str());
            argvv[1] = const_cast<char*> ("empty");
            int paramCounter = 2;
            while (getline(all_param, tmpParam, ',')) {
                //Trim current param
                trim(tmpParam);
                char* arg_copy = new char[tmpParam.size() + 1];
                strcpy(arg_copy, tmpParam.c_str());
                argvv[paramCounter++] = arg_copy;
            }
            argvv[paramsSize - 1] = NULL;

            commands[spawnedHosts] = const_cast<char*> (_mpiLauncherFile.c_str());
            argvs[spawnedHosts] = argvv;
            num_processes[spawnedHosts] = currHostInstances;

            totalNumberOfSpawns += currHostInstances;
            ++spawnedHosts;
        }
    }
#ifndef OPEN_MPI
    int fd = -1;
    //std::string lockname=NANOX_PREFIX"/bin/nanox-pfm";
    std::string lockname = "./.ompssOffloadLock";
    while (!nanos::ext::MPIProcessor::isDisableSpawnLock() && !shared && fd == -1) {
        fd = tryGetLock(const_cast<char*> (lockname.c_str()));
    }
#endif
    std::vector<MPI_Info> array_of_mpiinfo(host_info.begin(), host_info.end());

    //printf("(sergio)[%d] SPAWN count %d %d %s %d\n", getpid(), spawnedHosts, availableHosts, commands.front(), num_processes.front());
    //int len = commands.size();
    //for (int i=0, i< len; i++)
    //    printf("(sergio)[%d][%d/%d] SPAWN count %d\n", spawnedHosts);
    //*array_of_commands[], char **array_of_argv[],
    //                        const int array_of_maxprocs[], const MPI_Info array_of_info[],
    //                        int root, MPI_Comm comm, MPI_Comm *intercomm, int array_of_errcodes[]%d\n", getpid(), world_rank, world_size, spawnedHosts,&commands.front(),&argvs.front(), &num_processes.front(),&array_of_mpiinfo.front(), 0, comm, intercomm, MPI_ERRCODES_IGNORE);

    MPI_Comm_spawn_multiple(spawnedHosts,
            &commands.front(),
            &argvs.front(), &num_processes.front(),
            &array_of_mpiinfo.front(), 0, comm, intercomm, MPI_ERRCODES_IGNORE);
#ifndef OPEN_MPI
    if (!nanos::ext::MPIProcessor::isDisableSpawnLock() && !shared) {
        releaseLock(fd, const_cast<char*> (lockname.c_str()));
    }
#endif

    //Free all args sent
    for (int i = 0; i < spawnedHosts; i++) {
        //Free all args which were dynamically copied before
        for (int e = 2; argvs[i][e] != NULL; e++) {
            delete[] argvs[i][e];
        }
        delete[] argvs[i];
    }
}

void MPIRemoteNode::createNanoxStructures(MPI_Comm comm, MPI_Comm* intercomm, int spawnedHosts, int totalNumberOfSpawns, bool shared, int mpiSize, int currRank, int* pphList) {
    size_t maxWorkers = nanos::ext::MPIProcessor::getMaxWorkers();
    int spawn_start = 0;
    int numberOfSpawnsThisProcess = totalNumberOfSpawns;
    //If shared (more than one parent for this group), split total spawns between nodes in order to balance syncs
    if (shared) {
        numberOfSpawnsThisProcess = totalNumberOfSpawns / mpiSize;
        spawn_start = currRank*numberOfSpawnsThisProcess;
        if (currRank == mpiSize - 1) //Last process syncs the remaining processes
            numberOfSpawnsThisProcess += totalNumberOfSpawns % mpiSize;
    }

    PE * pes[totalNumberOfSpawns];
    //int uid=sys.getNumCreatedPEs();
    int arrSize;
    for (arrSize = 0; ompss_mpi_masks[arrSize] == MASK_TASK_NUMBER; arrSize++) {
    };
    int rank = spawn_start; //Balance spawn order so each process starts with his owned processes
    //Now they are spawned, send source ordering array so both master and workers have function pointers at the same position
    ext::SMPProcessor *core = sys.getSMPPlugin()->getLastFreeSMPProcessorAndReserve();
    if (core == NULL) {
        core = sys.getSMPPlugin()->getSMPProcessorByNUMAnode(0, getCurrentProcessor());
    }
    for (int rankCounter = 0; rankCounter < totalNumberOfSpawns; rankCounter++) {
        memory_space_id_t id = sys.getNewSeparateMemoryAddressSpaceId();
        SeparateMemoryAddressSpace *mpiMem = NEW SeparateMemoryAddressSpace(id, nanos::ext::MPI, nanos::ext::MPIProcessor::getAllocWide());
        mpiMem->setNodeNumber(0);
        sys.addSeparateMemory(id, mpiMem);
        //Each process will have access to every remote node, but only one master will sync each child
        //this way we balance syncs with childs
        if (rank >= spawn_start && rank < spawn_start + numberOfSpawnsThisProcess) {
            pes[rank] = NEW nanos::ext::MPIProcessor(intercomm, rank, 0/*uid++*/, true, shared, comm, core, id);
            ((MPIProcessor*) pes[rank])->setPphList(pphList);
            nanosMPISend(ompss_mpi_filenames, arrSize, MPI_UNSIGNED, rank, TAG_FP_NAME_SYNC, *intercomm);
            nanosMPISend(ompss_mpi_file_sizes, arrSize, MPI_UNSIGNED, rank, TAG_FP_SIZE_SYNC, *intercomm);
            //If user defined multithread cache behaviour, send the creation order
            if (nanos::ext::MPIProcessor::isUseMultiThread()) {
                cacheOrder order;
                order.opId = OPID_CREATEAUXTHREAD;
                nanosMPISend(&order, 1, nanos::MPIDevice::cacheStruct, rank, TAG_M2S_ORDER, *intercomm);
                ((MPIProcessor*) pes[rank])->setHasWorkerThread(true);
            }
        } else {
            pes[rank] = NEW nanos::ext::MPIProcessor(intercomm, rank, 0/*uid++*/, false, shared, comm, core, id);
            ((MPIProcessor*) pes[rank])->setPphList(pphList);
        }
        rank = (rank + 1) % totalNumberOfSpawns;
    }
    //Each node will have nSpawns/nNodes running, with a Maximum of 4
    //We supose that if 8 hosts spawns 16 nodes, each one will usually run 2
    //HINT: This does not mean that some remote nodes wont be accesible
    //using more than 1 thread is a performance tweak
    int numberOfThreads = (totalNumberOfSpawns / mpiSize);
    if (numberOfThreads < 1) numberOfThreads = 1;
    if (numberOfThreads > (int) maxWorkers) numberOfThreads = maxWorkers;
    BaseThread * threads[numberOfThreads];
    //start the threads...
    for (int i = 0; i < numberOfThreads; ++i) {
        NANOS_INSTRUMENT(sys.getInstrumentation()->incrementMaxThreads();)
        threads[i] = &((MPIProcessor*) pes[i])->startMPIThread(NULL);
    }
    sys.addPEsAndThreadsToTeam(pes, totalNumberOfSpawns, threads, numberOfThreads);
    nanos::ext::MPIPlugin::addPECount(totalNumberOfSpawns);
    nanos::ext::MPIPlugin::addWorkerCount(numberOfThreads);
    //Add all the PEs to the thread
    Lock* gLock = NULL;
    Atomic<unsigned int>* gCounter = NULL;
    std::vector<MPIThread*>* threadList = NULL;
    for (spawnedHosts = 0; spawnedHosts < numberOfThreads; spawnedHosts++) {
        MPIThread* mpiThread = (MPIThread*) threads[spawnedHosts];
        //Get the lock of one of the threads
        if (gLock == NULL) {
            gLock = mpiThread->getSelfLock();
            gCounter = mpiThread->getSelfCounter();
            threadList = mpiThread->getSelfThreadList();
        }
        threadList->push_back(mpiThread);
        mpiThread->addRunningPEs((MPIProcessor**) pes, totalNumberOfSpawns);
        //Set the group lock so they all share the same lock
        mpiThread->setGroupCounter(gCounter);
        mpiThread->setGroupThreadList(threadList);
        if (numberOfThreads > 1) {
            //Set the group lock so they all share the same lock
            mpiThread->setGroupLock(gLock);
        }
    }
    nanos::ext::MPIDD::setSpawnDone(true);
}

int MPIRemoteNode::nanosMPISendTaskinit(void *buf, int count, MPI_Datatype datatype, int dest,
        MPI_Comm comm) {
    cacheOrder order;
    order.opId = OPID_TASK_INIT;
    int* intbuf = (int*) buf;
    //Values of intbuf will be positive (using integer for conveniece with Fortran)
    order.hostAddr = *intbuf;
    //Send task init order (hostAddr=taskIdentifier)
    return nanosMPISend(&order, 1, nanos::MPIDevice::cacheStruct, dest, TAG_M2S_ORDER, comm);
}

int MPIRemoteNode::nanosMPIRecvTaskinit(void *buf, int count, MPI_Datatype datatype, int source,
        MPI_Comm comm, MPI_Status *status) {
    return nanosMPIRecv(buf, count, datatype, source, TAG_INI_TASK, comm, status);
}

int MPIRemoteNode::nanosMPISendTaskend(void *buf, int count, MPI_Datatype datatype, int disconnect,
        MPI_Comm comm) {
    if (_disconnectedFromParent) return 0;
    //Ignore destination (as is always parent) and get currentParent
    int res = nanosMPISend(buf, count, datatype, nanos::ext::MPIRemoteNode::getCurrentTaskParent(), TAG_END_TASK, comm);
    if (disconnect != 0) {
        MPI_Comm parent;
        MPI_Comm_get_parent(&parent);
        _disconnectedFromParent = true;
        MPI_Comm_disconnect(&parent);
    }
    return res;
}

int MPIRemoteNode::nanosMPIRecvTaskend(void *buf, int count, MPI_Datatype datatype, int source,
        MPI_Comm comm, MPI_Status *status) {
    //incrementNDatasent();
    //if (getNDatasent() == 2)
    //    printf("(sergio)[%d]: %s(%s,%d) @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n", getpid(), __FILE__, __func__, __LINE__);
    return nanosMPIRecv(buf, count, datatype, source, TAG_END_TASK, comm, status);
}

int MPIRemoteNode::nanosMPISendDatastruct(void *buf, int count, MPI_Datatype datatype, int dest,
        MPI_Comm comm) {
    nanosMPISend(buf, count, datatype, dest, TAG_ENV_STRUCT, comm);
    return 1;
}

int MPIRemoteNode::nanosMPIRecvDatastruct(void *buf, int count, MPI_Datatype datatype, int source,
        MPI_Comm comm, MPI_Status *status) {
    //Ignore destination (as is always parent) and get currentParent
    nanosMPIRecv(buf, count, datatype, nanos::ext::MPIRemoteNode::getCurrentTaskParent(), TAG_ENV_STRUCT, comm, status);
    nanosMPISendTaskend(buf, 1, MPI_BYTE, 1, comm);
    //printf("(sergio)[%d]: %s(%s,%d) data received - taskend sent\n", getpid(), __FILE__, __func__, __LINE__);
    return 0;
}

int MPIRemoteNode::nanosMPITypeCreateStruct(int count, int array_of_blocklengths[], MPI_Aint array_of_displacements[],
        MPI_Datatype array_of_types[], MPI_Datatype **newtype, int taskId) {
    int err;
    *newtype = NEW MPI_Datatype;
    _taskStructsCache[taskId] = *newtype;
    err = MPI_Type_create_struct(count, array_of_blocklengths, array_of_displacements, array_of_types, *newtype);
    ensure0(err == MPI_SUCCESS, "MPI Create struct failed when preparing the task. Please submit a ticket");
    err = MPI_Type_commit(*newtype);
    ensure0(err == MPI_SUCCESS, "MPI Create struct failed when preparing the task. Please submit a ticket");
    return err;
}

void MPIRemoteNode::nanosMPITypeCacheGet(int taskId, MPI_Datatype **newtype) {
    //Initialize cache if needed
    if (_taskStructsCache.size() == 0) {
        //Fill total number of tasks which have been compiled
        int arr_size;
        for (arr_size = 0; ompss_mpi_masks[arr_size] == MASK_TASK_NUMBER; arr_size++) {
        };
        unsigned int total_size = 0;
        for (int k = 0; k < arr_size; k++) total_size += ompss_mpi_file_ntasks[k];
        _taskStructsCache.assign(total_size, NULL);
    }
    ensure0(static_cast<int> (_taskStructsCache.size()) > taskId, "Tasks struct cache is failing, trying to access a taskId biggeer than the total number of tasks");
    *newtype = _taskStructsCache[taskId];
}

int MPIRemoteNode::nanosMPISend(void *buf, int count, MPI_Datatype datatype, int dest, int tag,
        MPI_Comm comm) {
    NANOS_MPI_CREATE_IN_MPI_RUNTIME_EVENT(ext::NANOS_MPI_SEND_EVENT);
    if (dest == UNKOWN_RANKSRCDST) {
        nanos::ext::MPIProcessor * myPE = (nanos::ext::MPIProcessor *) myThread->runningOn();
        dest = myPE->getRank();
        comm = myPE->getCommunicator();
    }
    //printf("Envio con tag %d, a %d\n",tag,dest);
    int err = MPI_Send(buf, count, datatype, dest, tag, comm);
    //printf("Fin Envio con tag %d, a %d\n",tag,dest);
    NANOS_MPI_CLOSE_IN_MPI_RUNTIME_EVENT;
    return err;
}

int MPIRemoteNode::nanosMPIIsend(void *buf, int count, MPI_Datatype datatype, int dest, int tag,
        MPI_Comm comm, MPI_Request *req) {
    NANOS_MPI_CREATE_IN_MPI_RUNTIME_EVENT(ext::NANOS_MPI_ISEND_EVENT);
    if (dest == UNKOWN_RANKSRCDST) {
        nanos::ext::MPIProcessor * myPE = (nanos::ext::MPIProcessor *) myThread->runningOn();
        dest = myPE->getRank();
        comm = myPE->getCommunicator();
    }
    //printf("Envio con tag %d, a %d\n",tag,dest);
    int err = MPI_Isend(buf, count, datatype, dest, tag, comm, req);
    //printf("Fin Envio con tag %d, a %d\n",tag,dest);
    NANOS_MPI_CLOSE_IN_MPI_RUNTIME_EVENT;
    return err;
}

int MPIRemoteNode::nanosMPISsend(void *buf, int count, MPI_Datatype datatype, int dest, int tag,
        MPI_Comm comm) {
    NANOS_MPI_CREATE_IN_MPI_RUNTIME_EVENT(ext::NANOS_MPI_SSEND_EVENT);
    if (dest == UNKOWN_RANKSRCDST) {
        nanos::ext::MPIProcessor * myPE = (nanos::ext::MPIProcessor *) myThread->runningOn();
        dest = myPE->getRank();
        comm = myPE->getCommunicator();
    }
    //printf("Enviobloq con tag %d, a %d\n",tag,dest);
    int err = MPI_Ssend(buf, count, datatype, dest, tag, comm);
    //printf("Fin Enviobloq con tag %d, a %d\n",tag,dest);
    NANOS_MPI_CLOSE_IN_MPI_RUNTIME_EVENT;
    return err;
}

int MPIRemoteNode::nanosMPIRecv(void *buf, int count, MPI_Datatype datatype, int source, int tag,
        MPI_Comm comm, MPI_Status *status) {
    NANOS_MPI_CREATE_IN_MPI_RUNTIME_EVENT(ext::NANOS_MPI_RECV_EVENT);
    if (source == UNKOWN_RANKSRCDST) {
        nanos::ext::MPIProcessor * myPE = (nanos::ext::MPIProcessor *) myThread->runningOn();
        source = myPE->getRank();
        comm = myPE->getCommunicator();
    }
    //printf("recv con tag %d, desde %d\n",tag,source);
    int err = MPI_Recv(buf, count, datatype, source, tag, comm, status);
    //printf("Fin recv con tag %d, desde %d\n",tag,source);
    NANOS_MPI_CLOSE_IN_MPI_RUNTIME_EVENT;
    return err;
}

int MPIRemoteNode::nanosMPIIRecv(void *buf, int count, MPI_Datatype datatype, int source, int tag,
        MPI_Comm comm, MPI_Request *req) {
    NANOS_MPI_CREATE_IN_MPI_RUNTIME_EVENT(ext::NANOS_MPI_IRECV_EVENT);
    if (source == UNKOWN_RANKSRCDST) {
        nanos::ext::MPIProcessor * myPE = (nanos::ext::MPIProcessor *) myThread->runningOn();
        source = myPE->getRank();
        comm = myPE->getCommunicator();
    }
    //printf("recv con tag %d, desde %d\n",tag,source);
    int err = MPI_Irecv(buf, count, datatype, source, tag, comm, req);
    //printf("Fin recv con tag %d, desde %d\n",tag,source);
    NANOS_MPI_CLOSE_IN_MPI_RUNTIME_EVENT;
    return err;
}

/**
 * Synchronizes host and device function pointer arrays to ensure that are in the same order
 * in both files (host and device, which are different architectures, so maybe they were not compiled in the same order)
 */
void MPIRemoteNode::nanosSyncDevPointers(int* file_mask, unsigned int* file_namehash, unsigned int* file_size,
        unsigned int* task_per_file, void *ompss_mpi_func_ptrs_dev[]) {
    MPI_Comm parentcomm; /* intercommunicator */
    MPI_Comm_get_parent(&parentcomm);
    //If this process was not spawned, we don't need this reorder (and shouldnt have been called)
    if (parentcomm != 0 && parentcomm != MPI_COMM_NULL) {
        //MPI_Status status;
        int arr_size;
        for (arr_size = 0; file_mask[arr_size] == MASK_TASK_NUMBER; arr_size++) {
        };
        unsigned int total_size = 0;
        for (int k = 0; k < arr_size; k++) {
            //Files which have 0 tasks, may add a NULL to the pointer array
            //if this is the case their number of tasks for reordering purposes is 1
            size_t element_offset = total_size * sizeof (void*);
            //All these many transformations are used to avoid warnings on gcc
            if (task_per_file[k] == 0 && ((void*) *(void**) (((uint64_t) ompss_mpi_func_ptrs_dev) + element_offset)) == NULL) task_per_file[k] = 1;
            total_size += task_per_file[k];
        }

        size_t filled_arr_size = 0;
        unsigned int* host_file_size = (unsigned int*) malloc(sizeof (unsigned int)*arr_size);
        unsigned int* host_file_namehash = (unsigned int*) malloc(sizeof (unsigned int)*arr_size);
        void (**ompss_mpi_func_pointers_dev_out)() = (void (**)()) malloc(sizeof (void (*)()) * total_size);
        //Receive host information
        nanos::ext::MPIRemoteNode::nanosMPIRecv(host_file_namehash, arr_size, MPI_UNSIGNED, MPI_ANY_SOURCE, TAG_FP_NAME_SYNC, parentcomm, MPI_STATUS_IGNORE);
        nanos::ext::MPIRemoteNode::nanosMPIRecv(host_file_size, arr_size, MPI_UNSIGNED, MPI_ANY_SOURCE, TAG_FP_SIZE_SYNC, parentcomm, MPI_STATUS_IGNORE);
        int i, e, func_pointers_arr;
        bool found;
        //i loops at host files
        for (i = 0; i < arr_size; i++) {
            func_pointers_arr = 0;
            found = false;
            //Search the host file in dev file and copy every pointer in the same order
            for (e = 0; !found && e < arr_size; e++) {
                if (file_namehash[e] == host_file_namehash[i] && file_size[e] == host_file_size[i]) {
                    found = true;
                    //Copy from _dev_tmp array to _dev array in the same order than the host
                    memcpy(ompss_mpi_func_pointers_dev_out + filled_arr_size, ompss_mpi_func_ptrs_dev + func_pointers_arr, task_per_file[e] * sizeof (void (*)()));
                    filled_arr_size += task_per_file[e];
                }
                func_pointers_arr += task_per_file[e];
            }
            fatal_cond0(!found, "Executable version mismatch, please compile the code using exactly the same sources (same filename, mod. date and size) for every architecture");
        }
        memcpy(ompss_mpi_func_ptrs_dev, ompss_mpi_func_pointers_dev_out, total_size * sizeof (void (*)()));
        free(ompss_mpi_func_pointers_dev_out);
        free(host_file_size);
        free(host_file_namehash);
    }
}
